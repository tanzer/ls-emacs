;-*- coding: utf-8; -*-

;;;; Copyright (C) 2022 Christian Tanzer All rights reserved
;;;; tanzer@gg32.com                                      https://www.gg32.com
;;;; #*** <License> ************************************************************#
;;;; This file is part of LS-Emacs, a package built on top of GNU Emacs.
;;;;
;;;; This file is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Library General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 2 of the License, or (at your option) any later version.
;;;;
;;;; This file is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;;; Library General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU Library General Public License
;;;; along with this file; if not, see <http://www.gnu.org/licenses/>.
;;;; #*** </License> ***********************************************************#
;;;;
;;;;++
;;;; Name
;;;;    lse-pos-ring
;;;;
;;;; Purpose
;;;;    Provide a ring of saved positions
;;;;
;;;; Revision Dates
;;;;    10-Feb-2022 (CT) Creation
;;;;    16-Feb-2022 (CT) Factor `lse-pos-ring:pred`
;;;;    16-Feb-2022 (CT) Factor `lse-pos-ring:add!`
;;;;    ««revision-date»»···
;;;;--

;;;+
;;; Ring of saved positions
;;;
;;; Scope:
;;; - buffer-local
;;; - global
;;; - overlay-specific (active context of edit-unit)
;;;
;;; Parameters:
;;; - size:   Maximum number of elements in the ring
;;; - pred:   Predicate used to decide whether to add a new element
;;; - cursor: Current index in ring during go operation
;;; - go-p :  Indicates a go operation is active
;;;-

;;; 10-Feb-2022
(defvar lse-pos-ring:default:size 30 "Default size of ring of positions.")

(defvar lse-pos-ring:par-map
  '((:type           0)
    (:ring           1)
    (:size           2)
    (:pred           3)
    (:cursor         4)
    (:go-p           5)
    (:desc           6)
   )
  "Map parameter names to indices in the record defining a ring of positions."
)

;;; 16-Feb-2022
(defun lse-pos-ring:add! (pr &optional pos)
  "Add position `pos` to ring of positions `pr` unconditionally."
  (let ((ring (lse-pos-ring:get pr :ring))
       )
    (ring-insert ring (or pos (point-marker)))
    (lse-pos-ring:reset:cursor pr)
  )
; lse-pos-ring:add!
)

;;; 10-Feb-2022
(defun lse-pos-ring:add (pr &optional pos)
  "Add position `pos` to ring of positions `pr` (default: (point-marker))."
  (let ((pred (lse-pos-ring:get pr :pred))
        (pos  (or pos (point-marker)))
        (last (lse-pos-ring:last pr))
       )
    (when (funcall pred pos last)
      (lse-pos-ring:add! pr pos)
    )
  )
; lse-pos-ring:add
)

;;; 10-Feb-2022
(defun lse-pos-ring:clear (pr)
  "Clear ring of positions `pr`."
  (lse-pos-ring:set pr :ring (make-ring (lse-pos-ring:get pr :size)))
  (lse-pos-ring:reset:cursor pr)
; lse-pos-ring:clear
)

;;; 10-Feb-2022
(defun lse-pos-ring:get (pr param)
  "Get parameter `param` from ring of positions `pr`."
  (aref pr (lse-assoc-value param lse-pos-ring:par-map))
; lse-pos-ring:get
)

;;; 10-Feb-2022
(defun lse-pos-ring:go (pr &optional np)
  "Goto a position in ring of positions `pr` (default: next older).

The prefix argument `-` means go to next younger position;
a numeric prefix means go to the position at index `np`,
with `0` referring to the youngest, `1` to the next youngest,
and `-1` to the oldest position."
  (let ((ring (lse-pos-ring:get pr :ring))
        (go-p (lse-pos-ring:get pr :go-p))
        (cur  (lse-pos-ring:get pr :cursor))
       )
    (unless (ring-empty-p ring)
      (unless go-p
        (let ((pm (point-marker))
             )
          (lse-pos-ring:add pr pm)
          (lse-pos-ring:set pr :go-p pm)
        )
      )
      (let* ((len (ring-length ring))
             (pos
               (mod
                 (cond
                   ((equal np '-) (1- cur)) ; go forward
                   ((numberp np)  np)       ; go to index `np`
                   (t             (1+ cur)) ; go backward
                 )
                 len
               ) ; normalize to range 0 .. len-1
             )
            )
        (goto-char (ring-ref ring pos))
        (lse-pos-ring:set pr :cursor pos)
        (message "Moved to saved position %d of %s" pos (1- len))
      )
    )
  )
; lse-pos-ring:go
)

;;; 10-Feb-2022
(defun lse-pos-ring:last (pr &optional index)
  "Return last element added to ring of positions `pr`."
  (let ((ring (lse-pos-ring:get pr :ring))
       )
    (unless (ring-empty-p ring)
      (ring-ref ring (or index 0))
    )
  )
; lse-pos-ring:last
)

;;; 10-Feb-2022
(defun lse-pos-ring:new (&optional size desc pred)
  "Return a new ring of positions."
  (let* ((size (or size lse-pos-ring:default:size))
         (desc (or desc "Ring of saved positions."))
         (pred (or pred 'lse-pos-ring:pred))
         (ring (make-ring size))
        )
    ;;      0             1    2    3    4 5   6
    (record 'lse-pos-ring ring size pred 0 nil desc)
  )
; lse-pos-ring:new
)

;;; 16-Feb-2022
(defun lse-pos-ring:pred (pos last)
  "Return `t` if position should be saved."
  (not (equal pos last))
; lse-pos-ring:pred
)

;;; 10-Feb-2022
(defun lse-pos-ring:reset:cursor (pr)
  "Reset `cursor` and `go-p` of ring of positions `pr`."
  (lse-pos-ring:set pr :cursor 0)
  (lse-pos-ring:set pr :go-p   nil)
; lse-pos-ring:reset:cursor
)

;;; 10-Feb-2022
(defun lse-pos-ring:set (pr param value)
  "Set parameter `param` from ring of positions `pr` to `value`."
  (aset pr (lse-assoc-value param lse-pos-ring:par-map) value)
; lse-pos-ring:set
)

(provide 'lse-pos-ring)

;;;; __END__ lse-pos-ring
