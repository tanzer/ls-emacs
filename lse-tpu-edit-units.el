;-*- coding: utf-8; -*-

;;;; Copyright (C) 2022 Christian Tanzer All rights reserved
;;;; tanzer@gg32.com                                      https://www.gg32.com
;;;; #*** <License> ************************************************************#
;;;; This file is part of the LS-Emacs, a package built on top of GNU Emacs.
;;;;
;;;; This file is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This file is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this file. If not, see <http://www.gnu.org/licenses/>.
;;;; #*** </License> ***********************************************************#
;;;;
;;;;++
;;;; Name
;;;;    lse-tpu-edit-units
;;;;
;;;; Purpose
;;;;    Provide edit-unit functions for lse-tpu
;;;;
;;;; Revision Dates
;;;;    17-Jan-2022 (CT) Start creation
;;;;    18-Jan-2022 (CT) Add functions handling `active-context`
;;;;    19-Jan-2022 (CT) Add `:add-goto-props`
;;;;    22-Jan-2022 (CT) Add functions defining keymaps and keys
;;;;    23-Jan-2022 (CT) Add support for `lse-tpu:last-position`
;;;;    23-Jan-2022 (CT) Add exit function, head-overlay
;;;;    23-Jan-2022 (CT) Improve functions defining keymaps and keys
;;;;                     + Add `f1` (describe-bindings) and `s-g` (exit)
;;;;                     + DRY key(map) definition/binding functions
;;;;    26-Jan-2022 (CT) Add `lse-tpu:eu:active-context:overlay`
;;;;                     * replaces `set-transient-map`
;;;;                     * add `keymap-parent`, move common key bindings to it
;;;;    26-Jan-2022 (CT) Add `lse-tpu:eu:repeat`, `lse-tpu:eu:select`
;;;;                     + Add `lse-tpu:eu:active-context:curr-overlay`
;;;;    26-Jan-2022 (CT) Add `cmd-mode-line`
;;;;    27-Jan-2022 (CT) Add `side` to `cmd-mode-line`
;;;;     1-Feb-2022 (CT) Define global `goto` bindings as commands, not prefix
;;;;                     + Allow switching between `goto` commands without
;;;;                       leaving active context
;;;;                     + Factor `lse-tpu:eu:active-context:re-init`
;;;;     2-Feb-2022 (CT) Add goto-commands for unit `char-oc`
;;;;     7-Feb-2022 (CT) Add goto-commands for units `cat-diff` and `defun`
;;;;     7-Feb-2022 (CT) Add goto-commands for units `list`, `down-list`,
;;;;                     and `up-list`
;;;;     7-Feb-2022 (CT) Add `saved-positions`
;;;;     8-Feb-2022 (CT) Add goto-commands for units `page` and `paragraph`
;;;;     9-Feb-2022 (CT) Add `lse-tpu:eu:unselect`
;;;;                     * Delegating `<cancel>` doesn't work because active
;;;;                       context remembers selection
;;;;    10-Feb-2022 (CT) Factor `lse-ring-pos`
;;;;    12-Feb-2022 (CT) Add cut/copy/paste commands
;;;;    14-Feb-2022 (CT) Add `lse-tpu:eu:replace-char`
;;;;                     + `lse-tpu:eu:replace-char-x`
;;;;    14-Feb-2022 (CT) Improve selection handling
;;;;                     + Change `lse-tpu:eu:select` to start selection at
;;;;                       `point`, not `:pos:start`
;;;;                     + Change `lse-tpu:eu:active-region` to consider
;;;;                       selection of active context
;;;;                     + Add `lse-tpu:eu:swap-selection`
;;;;    15-Feb-2022 (CT) Add select-unit commands
;;;;    17-Feb-2022 (CT) Add `region-overlay`
;;;;    19-Feb-2022 (CT) Add `ccp:unit/complete` commands
;;;;    23-Feb-2022 (CT) Use `buffer-substring-no-properties`, not
;;;;                     `buffer-substring` for cut/copy
;;;;    23-Feb-2022 (CT) Add `lse-tpu:eu:paste-cmd-list`
;;;;     5-Jun-2022 (CT) Factor `lse-tpu-eu--ccp-do-paste`
;;;;                     + Use `lse-tpu:undelete-region`
;;;;    ««revision-date»»···
;;;;--

;;;+
;;; Variables
;;;-
(defconst lse-tpu:eu:tl-cmd-list    () "Top-Level commands.")
(defconst lse-tpu:eu:ccp-cmd-list   () "Cut/copy/paste commands.")
(defconst lse-tpu:eu:paste-cmd-list () "Paste commands.")
(defconst lse-tpu:eu:goto-cmd-list  () "Goto commands.")

(defvar lse-tpu:eu:char-oc:unprefixed-target-chars
  "!#$%&)*+,-./:;<=>?@[\\]^_`({|}~§«»°'€\""
  "Characters bound to `lse-tpu:eu:goto-next:head:char-oc` ··· without prefix.

The list of characters is :

    '!' '#' '$' '%' '&' ')' '*' '+' ','
    '-' '.' '/' ':' ';' '<' '=' '>' '?'
    '@' '[' '\' ']' '^' '_' '`' '(' '{'
    '|' '}' '~' '§' '«' '»' '°' ''' '€' '\"'
"
)

(defvar lse-tpu:eu:d-region:delimiters "([{<«"
  "Delimiters for delimited regions."
)

(defvar lse-tpu:eu:wordlet:seps "-+*/|\_$%&.:,;?@#~=^"
  "Wordlet separators split a word into sub-units, e.g., `lse-tpu` comprises
the wordlets `lse` and `tpu`, separated by `-`."
)

(defvar lse-tpu:eu:goto-unit-key-chars
  (list
    (list "cat-diff"    "c")
    (list "down-list"   "d") ; down-list
    (list "defun"       "f") ; beginning-of-defun, end-of-defun
    (list "paragraph"   "g")
    (list "line"        "l")
    (list "page"        "p")
    (list "bs-word"     "s")
    (list "up-list"     "u") ; up-list, backward-up-list
    (list "wordlet"     "v") ; separated by `lse-tpu:eu:wordlet:seps`
    (list "word"        "w")
    (list "list"        "z")
    (list "char-oc"     lse-tpu:eu:char-oc:unprefixed-target-chars)
    (list "d-region"    lse-tpu:eu:d-region:delimiters)
  )
  "Key characters used to bind commands for the various edit-units."
)

(defvar lse-tpu:eu:cmd-mode-line:cat-map
  (list
    (list "goto-next"       "▶")
    (list "goto-prev"       "◀")
    (list "select-unit"     "S")
  )
   "Mode-line indicators for the commands `goto-next`, `goto-prev`, ···.

Override this to taste in your Emacs init-file, e.g., ~/.emacs."
)

(defconst lse-tpu:eu:cmd-categories
  (mapcar 'car lse-tpu:eu:cmd-mode-line:cat-map)
  "Categories of edit-unit commands.

 Automatically computed from 'lse-tpu:eu:cmd-mode-line:cat-map'."
)

(defconst lse-tpu:eu:cmd-categories:regexp
  (concat
    ":\\("
    (mapconcat
      'identity
      lse-tpu:eu:cmd-categories
      "\\|"
    )
    "\\)"
  )
  "Regular expression matching a command category in a command name.

 Automatically computed from 'lse-tpu:eu:cmd-categories'."
)

(defconst lse-tpu:eu:tl-cmd:regexp
  (concat
    lse-tpu:eu:cmd-categories:regexp "\\(?::\\(head\\|tail\\)\\)?"
  )
  "Regular expression matching command category and side of a command name."
)

(defconst lse-tpu:eu:cmd:regexp
  (concat
    lse-tpu:eu:tl-cmd:regexp ":\\(.*\\)"
  )
  "Regular expression matching command category, side, and unit of a command
name."
)

(defconst lse-tpu:eu:ccp-cmd:regexp
  (concat
    ":\\(cut\\|copy\\|paste\\):\\(.*\\)$"
  )
  "Regular expression matching unit of a cut/copy/paste command name."
)

(defvar lse-tpu:eu:cmd-mode-line:side-map
  (list
    (list "curr"            "<>")
    (list "head"            "❮")
    (list "tail"            "❯")
  )
   "Mode-line indicators for the command sides.

Override this to taste in your Emacs init-file, e.g., ~/.emacs."
)

;;;+
;;; Keymaps and functions to bind edit-unit commands to keys
;;;-
(defvar lse-tpu:eu:top-level-cmd-keys
  (list
    (list "goto-next:head"       [(super ?f)])
    (list "goto-next:tail"       [(super ?g)])
    (list "goto-prev:head"       [(super ?a)])
    (list "goto-prev:tail"       [(super ?b)])
    (list "select-unit"          [(super ?s)])
  )
   "Keys bound to the top-level edit-unit commands, e.g., `goto-next:head`.

Override this to taste in your Emacs init-file, e.g., ~/.emacs."
)

;;; 12-Feb-2022
(defvar lse-tpu:eu:ccp-prefix-keys
  (list
    (list "cut"         [(super ?x)])
    (list "copy"        [(super ?c)])
    (list "paste"       [(super ?v)])
  )
  "Map of prefix keys for cut/copy/paste commands.

Override this to taste in your Emacs init-file, e.g., ~/.emacs."
)

(defconst lse-tpu:eu:ccp-prefixes
  (mapcar (lambda (l) (cadr l)) lse-tpu:eu:ccp-prefix-keys)
  "Prefix keys for cut/copy/paste commands."
)

;;; 12-Feb-2022
(defconst lse-tpu-eu--ccp-unit-keys
  (list
    (list "char"        "bcd")
    (list "line"        "klm")
    (list "word"        "vwx")
    (list "region"      "123456789a<,.efghij-#+nopqrstuäöüyz")
  )
  "Keys bound to edit units for cut/copy/paste commands. Each key refers to
a different ccp-buffer of its unit.

For instance: for `char`, key `b` refers to char-buffer 1, key `c` refers to
char-buffer 2, etc."
)

(defconst lse-tpu-eu--ccp-unit-key-hash nil
  "Hash table mapping keys to ccp-unit and ccp-buffer-index."
)

;;; 19-Feb-2022
(defconst lse-tpu-eu--ccp-delete-fct-map
  (list
    (list "char"   'lse-tpu:delete-char)
    (list "line"   'lse-tpu:delete-line)
    (list "region"
      (lambda (head tail dir append)
        (lse-tpu:delete-entity head tail dir append
          (lse-tpu:ccp-buffer:active :region)
        )
      )
    )
    (list "word"   'lse-tpu:delete-word)
   )
)

;;; 19-Feb-2022
(defconst lse-tpu-eu--ccp-undelete-fct-map
  (list
    (list "char"   'lse-tpu:undelete-char)
    (list "line"   'lse-tpu:undelete-line)
    (list "region" 'lse-tpu:undelete-region)
    (list "word"   'lse-tpu:undelete-word)
   )
)

;;; 29-Jan-2022
(defvar lse-tpu:eu:char-oc-prefix-keys
  (list
    (list "next-oc"         [?b])
    (list "prev-oc"         [?a])
  )
  "Prefix keys bound to the edit-unit parent table for commands going to the
next/previous occurrence of a character."
)

;;; 15-Feb-2022
(defvar lse-tpu:eu:char-region-prefix-key [?r]
  "Prefix kex for select-unit:char-region"
)

;;; 28-Jan-2022
(defvar lse-tpu:eu:keys-to-delegate
  '( [left] [right] [up] [down] [end] [home]
     ;;+
     ;; Key sequences with translated symbols like <gold>··· don't work
     ;; - for some reason, the unstranslated key, i.e., <f12> not <gold> is
     ;;   visible in ···delegate and ···exit+delegate
     ;; - worse, instead of [gold backspace], only [f12] is passed to
     ;;   ···delegate --> lookup-key returns a keymap, not a command
     ;;-
     [backspace]
     [delete]
     [?\C-_]         ; undo
     [?\C-l]         ; recenter-top-bottom
     [?\C-<] [?\C->] ; lse-tpu:goto-next-occurrence-current-char, ···prev···
     [?\C-0] [?\C-1] [?\C-2] [?\C-3] [?\C-4] [?\C-5]
     [?\C-6] [?\C-7] [?\C-8] [?\C-9] [?\C--] [?\C-u]
     [?\s-r] [?\C-\s-r]
   )
   "Defines keys to be delegated to normal bindings.

Override this to taste in your Emacs init-file, e.g., ~/.emacs."
)

(defvar lse-tpu:eu:keymap:parent
  (make-sparse-keymap "edit-unit parent table")
  "Parent keymap for all edit-unit keymaps."
)

(lse-define-prefix-command 'lse-tpu:eu:keymap:goto-next:head "goto-next:head"
  "Keymap for commands that go to the next beginning of an edit-unit."
)

(lse-define-prefix-command 'lse-tpu:eu:keymap:goto-next:tail "goto-next:tail"
  "Keymap for commands that go to the next end of an edit-unit."
)

(lse-define-prefix-command 'lse-tpu:eu:keymap:goto-prev:head "goto-prev:head"
  "Keymap for commands that go to the previous beginning of an edit-unit."
)

(lse-define-prefix-command 'lse-tpu:eu:keymap:goto-prev:tail "goto-prev:tail"
  "Keymap for commands that go to the previous end of an edit-unit."
)

(lse-define-prefix-command 'lse-tpu:eu:keymap:select-unit "select-unit"
  "Keymap for commands selecting an edit-unit around point."
)

(lse-define-prefix-command 'lse-tpu:eu:keymap:select-unit:char-region
  "select-unit:char-region"
  "Keymap for commands selecting a character-delimited region around point."
)

(lse-define-prefix-command 'lse-tpu:eu:keymap:char:next-oc "goto-next:char-oc"
  "Keymap bound to 'lse-tpu:goto-next-char."
)

(lse-define-prefix-command 'lse-tpu:eu:keymap:char:prev-oc "goto-prev:char-oc"
  "Keymap bound to 'lse-tpu:goto-prev-char."
)

;;; 23-Jan-2022
(defun lse-tpu:eu:keymap-of-cmd (cmd)
  "Return the keymaps where `cmd` is bound."
  (get cmd 'lse:keymap)
; lse-tpu:eu:keymap-of-cmd
)

;;;  2-Feb-2022
(defun lse-tpu:eu:key-desc (evt)
  (key-description (if (vectorp evt) evt (vector evt)))
; lse-tpu:eu:key-desc
)

;;; 23-Jan-2022
(defun lse-tpu:eu:event-type-desc (event-type)
  (let* ((ebt (event-basic-type event-type))
         (ems (event-modifiers  event-type))
        )
    (when (integerp ebt)
      (setq ebt (format "?%s" (char-to-string ebt)))
    )
    (when (symbolp ebt)
      (setq ebt (symbol-name ebt))
    )
    (format "(%s)"
      (if ems
          (mapconcat
            (lambda (x) (format "%s" x))
            (append ems (if (stringp ebt) (list ebt) ebt) ()) " "
          )
        ebt
      )
    )
  )
; lse-tpu:eu:event-type-desc
)

;;; 24-Jan-2022
(defun lse-tpu:eu:event-type-seq-desc (keyseq)
  (format "[%s]" (mapconcat 'lse-tpu:eu:event-type-desc keyseq " "))
; lse-tpu:eu:event-type-seq-desc
)

;;; 23-Jan-2022
(defun lse-tpu:eu:keys-of-keymap (map)
  "Return all keys bound in `map'."
  (let ((result ())
       )
    (map-keymap
      (lambda (event-type binding)
        (add-to-list 'result
          (lse-tpu:eu:event-type-desc event-type)
        )
      )
      map
    )
    result
  )
; lse-tpu:eu:keys-of-keymap
)

;;; 26-Jan-2022
(defun lse-tpu:eu:define-parent-keymap ()
  "Define key bindings for 'lse-tpu:eu:keymap:parent'"
  (let ((map         lse-tpu:eu:keymap:parent)
        (global-exit [(control super ?g)])
        (swap-select [(control super ?.)])
       )
    (global-set-key global-exit     'lse-tpu:eu:active-context:exit)
    (define-key map [f1]            'lse-tpu:eu:describe-bindings)
    (define-key map [escape escape] 'lse-tpu:eu:active-context:exit)
    (define-key map [select]        'lse-tpu:eu:select)
    (define-key map swap-select     'lse-tpu:eu:swap-selection)
    (define-key map [cancel]        'lse-tpu:eu:unselect)
    (define-key map [(control ?g)]  'lse-tpu:eu:active-context:exit)
    (define-key map [(super   ?z)]  'lse-tpu:eu:saved-positions:go)
    (define-key map [?\s]           'lse-tpu:eu:repeat)
    ;; undefined keys end an active context
    (define-key map [t]             'lse-tpu:eu:active-context:exit+delegate)
    ;; keys to delegate
    (mapc
      (lambda (key)
         (define-key map key (lse-tpu:eu:delegated-cmd key))
      )
      lse-tpu:eu:keys-to-delegate
    )
    ;; top-level command keys
    (mapc (lambda (cmd) (define-key map (get cmd 'lse:cmd-key) cmd))
      lse-tpu:eu:tl-cmd-list
    )
    ;; char-region
    (let ((s-map lse-tpu:eu:keymap:select-unit)
          (r-map lse-tpu:eu:keymap:select-unit:char-region)
          (key   lse-tpu:eu:char-region-prefix-key)
         )
      (define-key s-map key r-map)
      (define-key           r-map [t] 'lse-tpu:eu:select-unit:char-region)
    )
    ;; char-oc
    (let ((n-key (lse-assoc-value "next-oc" lse-tpu:eu:char-oc-prefix-keys))
          (p-key (lse-assoc-value "prev-oc" lse-tpu:eu:char-oc-prefix-keys))
          (n-map lse-tpu:eu:keymap:char:next-oc)
          (p-map lse-tpu:eu:keymap:char:prev-oc)
         )
      (define-key map n-key n-map)
      (define-key map p-key p-map)
      (define-key           n-map [t] 'lse-tpu:eu:goto-next:head:char-oc)
      (define-key           p-map [t] 'lse-tpu:eu:goto-prev:head:char-oc)
    )
    ;; cut/copy/paste commands
    (mapc (lambda (k) (global-unset-key k))   lse-tpu:eu:ccp-prefixes)
    (mapc (lambda (k) (define-key map k nil)) lse-tpu:eu:ccp-prefixes)
    (lse-tpu:eu:define-ccp-keys (current-global-map))
    (lse-tpu:eu:define-ccp-keys map)
    ;; define numeric prefix keys
    (mapc
      (lambda (k)
        (define-key map (vector (list 'super k)) 'digit-argument)
      )
      '(?0 ?1 ?2 ?3 ?4 ?5 ?6 ?7 ?8 ?9)
    )
    (define-key map [(super ?-)] 'negative-argument)
  )
; lse-tpu:eu:define-parent-keymap
)

;;; 12-Feb-2022
(defun lse-tpu:eu:define-ccp-keys-cmd (cmd keymap)
  "Define keys bound to cut/copy/paste command `cmd` in `keymap`."
  (let* ((prefix  (get cmd 'lse:prefix))
         (keys    (get cmd 'lse:keys))
         (keyseqs (mapcar (lambda (k) (vconcat prefix k)) keys))
        )
    (mapc
      (lambda (ks) (define-key keymap ks cmd))
      keyseqs
    )
  )
; lse-tpu:eu:define-ccp-keys-cmd
)

;;; 12-Feb-2022
(defun lse-tpu:eu:define-ccp-keys (keymap)
  "Define cut/copy/paste commands in keymap."
  (mapc
    (lambda (cmd) (lse-tpu:eu:define-ccp-keys-cmd cmd keymap))
    lse-tpu:eu:ccp-cmd-list
  )
; lse-tpu:eu:define-ccp-keys
)

;;; 23-Jan-2022
(defun lse-tpu:eu:bind-tl-cmd+keymap (cmd)
  "Bind top-level command `cmd' to its key, prepare its keymap."
  (let ((key    (get cmd 'lse:cmd-key))
        (keymap (get cmd 'lse:keymap))
       )
    (global-set-key key cmd)
    (set-keymap-parent keymap lse-tpu:eu:keymap:parent)
  )
; lse-tpu:eu:bind-tl-cmd+keymap
)

;;; 22-Jan-2022
(defun lse-tpu:eu:define-tl-cmd-keys+keymaps ()
  "Define the key bindings for top-level edit-units commands."
  (mapc 'lse-tpu:eu:bind-tl-cmd+keymap lse-tpu:eu:tl-cmd-list)
; lse-tpu:eu:define-tl-cmd-keys+keymaps
)

;;; 23-Jan-2022
(defun lse-tpu:eu:bind-key-to-cmd (key cmd)
  "Bind `key' to `cmd'."
  (let* ((tl-cmd (global-key-binding (get cmd 'lse:cmd-key)))
         (keymap (get tl-cmd 'lse:keymap))
        )
    (when keymap
      (define-key keymap key cmd)
    )
  )
; lse-tpu:eu:bind-key-to-cmd
)

;;; 22-Jan-2022
(defun lse-tpu:eu:define-goto-keys-cmd (cmd)
  "Define keys bound to lse-tpu edit-unit `cmd'."
  (mapc
    (lambda (key) (lse-tpu:eu:bind-key-to-cmd key cmd))
    (get cmd 'lse:keys)
  )
  (mapc
    (lambda (key) (lse-tpu:eu:bind-key-to-cmd key cmd))
    (get cmd 'lse:extra-keys)
  )
; lse-tpu:eu:define-goto-keys-cmd
)

;;; 22-Jan-2022
(defun lse-tpu:eu:define-goto-keys ()
  "Define keys for lse-tpu edit-unit commands."
  (mapc 'lse-tpu:eu:define-goto-keys-cmd lse-tpu:eu:goto-cmd-list)
; lse-tpu:eu:define-goto-keys
)

;;; 22-Jan-2022
(defun lse-tpu:eu:define-keymaps+keys ()
  "Define keymaps and keys bound to lse-tpu edit-unit commands."
  (lse-tpu:eu:define-tl-cmd-keys+keymaps)
  (lse-tpu:eu:define-parent-keymap)
  (lse-tpu:eu:define-goto-keys)
; lse-tpu:eu:define-keymaps+keys
)

;;;  4-Feb-2022
(defun lse-tpu:eu:translated-event (event)
  (let* ((t-maps
           (list
             input-decode-map
             local-function-key-map
             key-translation-map
           )
         )
         (ev-vector (if (vectorp event) event (vector event)))
         (translation
           (apply 'vconcat
             (mapcar
               (lambda (key)
                 (let* ((v-key (if (vectorp key) key (vector key)))
                        (t-key
                          (unless (equal v-key [backspace])
                            (lookup-key t-maps v-key)
                          )
                        )
                        (result (or t-key key))
                       )
                   (if (vectorp result) result (vector result))
                 )
               )
               ev-vector
             )
           )
         )
        )
    translation
  )
; lse-tpu:eu:translated-event
)

;;;  2-Feb-2022
(defun lse-tpu:eu:delegated-cmd (event)
  "Command bound to `event` in the local/global keymaps."
  (let* ((maps
           (append
             (current-minor-mode-maps)
             (current-local-map)
             (current-global-map)
             ()
           )
         )
         (key (lse-tpu:eu:translated-event event))
         (cmd (lookup-key maps key))
        )
    cmd
  )
; lse-tpu:eu:delegated-cmd
)

;;;  2-Feb-2022
(defun lse-tpu:eu:format-binding (evt binding &optional prefix)
  "Format `binding` of `evt`"
  (let* ((eff-binding
          (cond
            ((eq binding 'lse-tpu:eu:active-context:delegate)
             (or (lse-tpu:eu:delegated-cmd evt) binding)
            )
            (t
             binding
            )
          )
         )
         (evt-desc (lse-tpu:eu:key-desc evt))
         (cmd-name (get eff-binding 'lse:cmd-name))
         (cmd-doc  (when eff-binding (documentation eff-binding)))
         (bnd-desc
           (or
             (when cmd-doc
               (replace-regexp-in-string "\n.*" "" cmd-doc)
             )
             cmd-name
             eff-binding
           )
         )
        )
    (format "%s%-20s %s" prefix evt-desc bnd-desc)
  )
; lse-tpu:eu:format-binding
)

;;; 23-Jan-2022
(defun lse-tpu:eu:format-bindings (keymap &optional prefix)
  "Describe bindings of keymap."
  (let* ((prefix (or prefix ""))
         (result ())
        )
    (map-keymap
      (lambda (evt binding)
        (add-to-list 'result
          (if (keymapp binding)
              (lse-tpu:eu:format-bindings binding
                (concat prefix (lse-tpu:eu:key-desc evt) " ")
              )
            (lse-tpu:eu:format-binding evt binding prefix)
          )
        )
      )
      keymap
    )
    (mapconcat 'identity result "\n")
  )
; lse-tpu:eu:format-bindings
)

(defun lse-tpu:eu:describe-bindings ()
  "Describe bindings of keymap."
  (interactive)
  (when lse-tpu:eu:active-context
    (let* ((cmd      (get lse-tpu:eu:active-context 'lse:cmd))
           (cmd-name (get cmd 'lse:cmd-name))
           (keymap   (lse-tpu:eu:keymap-of-cmd cmd))
           (bindings (lse-tpu:eu:format-bindings keymap))
           (help-buf (help-buffer))
           (case-fold-search nil)
           head-pos
          )
      (with-help-window help-buf
        (with-current-buffer help-buf
          (insert (format "Help for %s\n\n" cmd-name))
          (setq head-pos (point))
          (insert bindings)
          (lse-tpu:replace-all " <t>            " " <any undef key>")
          ;; sort the output
          ;; - add `~` at the beginning to sort keys with modifiers and
          ;;   function keys below plain characters
          (lse-tpu:replace-all "^<t>            "
                               "~<any undef key>")           ; default bindings
          (lse-tpu:replace-all "^<\\([^ ]\\)" "~~~~~<\\1")   ; function keys at end
          (lse-tpu:replace-all "^\\([ACMs]-\\)" "~~~~\\1")   ; modifiers after chars
          (lse-tpu:replace-all "^\\([^a-zA-Z] \\)" "~~~\\1") ; non-letters
          (lse-tpu:replace-all "^\\([A-Z]+ \\)" "~~\\1")     ; upper-case letters
          (sort-lines nil head-pos (point))
          (lse-tpu:replace-all "^~~ "          "~ ")  ; remove sort prio keys
          (lse-tpu:replace-all "^~+\\([^ ]\\)" "\\1") ; remove sort prio keys
        )
      )
    )
  )
; lse-tpu:eu:describe-bindings
)

;;;+
;;; Scaffolding
;;;-
;;; 27-Jan-2022
(defun lse-tpu:eu:cmd-mode-line:indicator (cmd-cat side)
   (let ((dir-ind  (lse-assoc-value cmd-cat lse-tpu:eu:cmd-mode-line:cat-map))
         (side-ind (lse-assoc-value side    lse-tpu:eu:cmd-mode-line:side-map))
         result
        )
     (setq result
       (concat
         (when side-ind
           (if (equal side "head")
               side-ind
             (when (equal side "curr") (concat (substring side-ind 0 1) " "))
           )
         )
         dir-ind
         (when side-ind
           (if (equal side "curr")
               (concat " " (substring side-ind 1))
             (when (equal side "tail") side-ind)
           )
         )
       )
     )
     result
   )
; lse-tpu:eu:cmd-mode-line:indicator
)

;;;  1-Feb-2022
(defun lse-tpu-eu--add-goto-props (cmd &rest args)
  (save-match-data
    (when args
      (apply 'lse-put-sym-props cmd args)
    )
    (let* ((name      (symbol-name cmd))
           (match     (string-match lse-tpu:eu:tl-cmd:regexp name))
           (cmd-cat   (match-string 1 name))
           (side      (match-string 2 name))
           (cmd-dir
             (if (string-match "-prev:" cmd-cat)
                 'lse-tpu:direction-backward
               'lse-tpu:direction-forward
             )
           )
           (cmd-name  (if side (concat cmd-cat ":" side) cmd-cat))
           (cmd-mli
             (lse-tpu:eu:cmd-mode-line:indicator cmd-cat (or side "curr"))
           )
           (cmd-key   (lse-assoc-value cmd-name lse-tpu:eu:top-level-cmd-keys))
          )
      (lse-put-sym-props cmd
        'lse:cmd-cat     cmd-cat
        'lse:cmd-dir     cmd-dir
        'lse:cmd-name    cmd-name
        'lse:side        side
        'lse:cmd-mli     cmd-mli
        'lse:cmd-key     cmd-key
      )
    )
  )
; lse-tpu-eu--add-goto-props
)

(defun lse-tpu:eu:add-tlg-props (cmd &rest args)
  "Add symbol properties to edit-unit top-level goto command `cmd'."
  (apply 'lse-tpu-eu--add-goto-props cmd args)
  (add-to-list 'lse-tpu:eu:tl-cmd-list cmd)
; lse-tpu:eu:add-goto-props
)

;;; 19-Jan-2022
(defun lse-tpu:eu:add-goto-props (cmd &rest args)
  "Add symbol properties to edit-unit goto command `cmd'."
  (save-match-data
    (apply 'lse-tpu-eu--add-goto-props cmd args)
    (let* ((name      (symbol-name cmd))
           (match     (string-match lse-tpu:eu:cmd:regexp name))
           (unit      (match-string 3 name))
           (key-chars (lse-assoc-value unit lse-tpu:eu:goto-unit-key-chars))
           (keys      (mapcar 'vector (mapcar 'abs key-chars)))
          )
      (lse-put-sym-props cmd
        'lse:cmd-name    (concat (get cmd 'lse:cmd-name) ":" unit)
        'lse:key-chars   key-chars
        'lse:keys        keys
        'lse:unit-name   unit
      )
      (add-to-list 'lse-tpu:eu:goto-cmd-list cmd)
    )
  )
; lse-tpu:eu:add-goto-props
)

;;;  9-Feb-2022
(defun lse-tpu:eu:add-ccp-completion-props (cmd &rest args)
  "Add symbol properties to edit-unit cut/copy/paste command `cmd`."
  (save-match-data
    (when args
      (apply 'lse-put-sym-props cmd args)
    )
    (let* ((name      (symbol-name cmd))
           (match     (string-match lse-tpu:eu:ccp-cmd:regexp name))
           (cmd-cat   (match-string 1 name))
           (unit      (match-string 2 name))
           (cmd-name  (concat cmd-cat ":" unit))
           (prefix    (lse-assoc-value cmd-cat lse-tpu:eu:ccp-prefix-keys))
          )
      (lse-put-sym-props cmd
        'lse:cmd-cat     cmd-cat
        'lse:cmd-name    cmd-name
        'lse:keys        [[??]]
        'lse:prefix      prefix
        'lse:unit-name   unit
      )
      (add-to-list 'lse-tpu:eu:ccp-cmd-list cmd)
      (when (string-equal cmd-cat "paste")
        (add-to-list 'lse-tpu:eu:paste-cmd-list cmd)
      )
    )
  )
; lse-tpu:eu:add-ccp-completion-props
)

;;;+
;;; Saved positions
;;;-
(defvar lse-tpu:eu:saved-positions:num 30)

(defvar-local lse-tpu:eu:saved-positions nil
  "Ring of saved positions for goto edit-unit command."
)

;;; 10-Feb-2022
(defun lse-tpu:eu:saved-positions:new ()
  "Return a new ring of saved positions for goto edit-unit command."
  (lse-pos-ring:new lse-tpu:eu:saved-positions:num
    "Ring of saved positions for goto edit-unit command."
  )
; lse-tpu:eu:saved-positions:new
)

;;;  7-Feb-2022
(defun lse-tpu:eu:saved-positions:go (&optional np)
  "Goto a position in ring of saved positions (default: next older).

The prefix argument `-` means go to next younger position;
a numeric prefix means go to the position at index `np`,
with `0` referring to the youngest, `1` to the next youngest,
and `-1` to the oldest position.
"
  (interactive "P")
  (lse-pos-ring:go lse-tpu:eu:saved-positions np)
; lse-tpu:eu:saved-positions:go
)

;;;+
;;; Active context during edit-unit modes
;;;-
(defvar lse-tpu:eu:active-context nil
  "Hold information necessary during the execution edit-unit movement commands.

When an edit-unit movement command is started, its keymap is
defined as a temporary overlay's  keymap. This way,
movement commands can be repeated and combined without repeating its starting
command key.

The edit-unit movement commands all support shift-selection.

Therefore, `lse-tpu:eu:active-context:init` adds
`lse-tpu:eu:active-context:update` to `post-command-hook` to make an
initial shift-selection work without repeating shift-selection for every
subsequent movement command.

`lse-tpu:eu:active-context:init` puts various properties on
`lse-tpu:eu:active-context` to store the information necessary to
support `shift-selection`, `repeat`, and the passing on of the
numeric argument `num`."
)

(defvar-local lse-tpu:eu:active-context:overlay nil
  "Buffer-wide overlay providing the command keymap  of an active context."
)
(defvar-local lse-tpu:eu:active-context:curr-overlay nil
  "Overlay showing the region between the bound and current positions of an
active context."
)
(defvar-local lse-tpu:eu:active-context:head-overlay nil
  "Overlay showing the start position of an active context."
)
(defvar-local lse-tpu:eu:active-context:region-overlay nil
  "Overlay showing the selected region of an active context."
)

(lse-face:define 'lse-tpu:eu:active-context:overlay:face
  "light yellow" nil
)

(lse-face:define 'lse-tpu:eu:active-context:curr-overlay:face
  "light cyan" nil
)

(lse-face:define 'lse-tpu:eu:active-context:head-overlay:face
  "Gray60" "Gray90"
)

;;; 18-Jan-2022
(defun lse-tpu:eu:active-context:init (num)
  "Initialize a new `lse-tpu:eu:active-context` for a edit-unit goto."
  (unless lse-tpu:eu:active-context
    (let* ((shifted   this-command-keys-shift-translated)
           (start-pos (copy-marker (point-marker)))
          )
      (setq lse-tpu:eu:active-context (make-symbol ":eu:active-context"))
      (if lse-tpu:eu:saved-positions
          (lse-pos-ring:clear lse-tpu:eu:saved-positions)
        (setq lse-tpu:eu:saved-positions (lse-tpu:eu:saved-positions:new))
      )
      (lse-put-sym-props lse-tpu:eu:active-context
         'lse:pos:mark   nil
         'lse:pos:bound  start-pos
         'lse:pos:start  start-pos
         'lse:shifted    shifted
      )
      (let ((overlay (make-overlay (point-min) (point-max)))
           )
        (setq lse-tpu:eu:active-context:overlay overlay)
        (overlay-put
          overlay 'face 'lse-tpu:eu:active-context:overlay:face
        )
      )
      (setq lse-tpu:eu:active-context:curr-overlay
        (make-overlay start-pos (1+ start-pos))
      )
      (overlay-put
        lse-tpu:eu:active-context:curr-overlay
        'face 'lse-tpu:eu:active-context:curr-overlay:face
      )
      (setq lse-tpu:eu:active-context:head-overlay
        (make-overlay start-pos (1+ start-pos))
      )
      (overlay-put
        lse-tpu:eu:active-context:head-overlay
        'face 'lse-tpu:eu:active-context:head-overlay:face
      )
      (overlay-put lse-tpu:eu:active-context:head-overlay 'priority 9)
      (add-hook 'post-command-hook 'lse-tpu:eu:active-context:update)
    )
  )
  (lse-tpu:eu:active-context:re-init num)
; lse-tpu:eu:active-context:init
)

;;;  1-Feb-2022
(defun lse-tpu:eu:active-context:re-init (num)
  "Re-initialize an existing `lse-tpu:eu:active-context` for a edit-unit goto."
  (let* ((cmd       this-command)
         (cmd-vec   (this-command-keys-vector))
         (map       (lse-tpu:eu:keymap-of-cmd cmd))
         (map-name  (keymap-prompt map))
         (cmd-mli   (get cmd 'lse:cmd-mli))
         (overlay   lse-tpu:eu:active-context:overlay)
        )
    (unless num
      (setq num     (get lse-tpu:eu:active-context 'lse:num))
    )
    (lse-put-sym-props lse-tpu:eu:active-context
       'lse:cmd        cmd
       'lse:cmd-vec    cmd-vec
       'lse:map-name   map-name
       'lse:num        (prefix-numeric-value num)
    )
    (overlay-put overlay 'keymap map)
    (setq lse-tpu:edit-unit:mode-line (or cmd-mli ""))
    (message
      "You can use all the keys of the keymap `%s  %s`."
      map-name lse-tpu:edit-unit:mode-line
    )
    (lse-tpu:eu:active-context:set-cmd nil num)
  )
; lse-tpu:eu:active-context:re-init
)

;;;  1-Feb-2022
(defun lse-tpu:eu:active-context:set-cmd (cmd num)
  "Set property 'lse:last-cmd of active context to current command."
  (when lse-tpu:eu:active-context
    (put lse-tpu:eu:active-context 'lse:last-cmd cmd)
  )
; lse-tpu:eu:active-context:set-cmd
)

(defun lse-tpu:eu:active-context--delegate (&optional before-fct after-fct)
  (let* ((event (this-command-keys-vector))
         (cmd   (lse-tpu:eu:delegated-cmd event))
        )
    (when cmd
      (if (keymapp cmd)
          (message "Event %s resolves to keymap" event)
        (when before-fct (funcall before-fct event cmd))
        (setq this-command  cmd)
        (call-interactively cmd)
        (when after-fct  (funcall after-fct  event cmd))
      )
    )
  )
; lse-tpu:eu:active-context--delegate
)

(defun lse-tpu:eu:active-context:delegate ()
  "Delegate key-binding to global keymap but stay in active edit-unit context."
  (interactive)
  (lse-tpu:eu:active-context--delegate
    (lambda (event cmd) (lse-tpu:eu:active-context:set-cmd cmd nil))
    (lambda (event cmd) (lse-tpu:eu:active-context:update))
  )
; lse-tpu:eu:active-context:delegate
)

;;; 23-Jan-2022
(defun lse-tpu:eu:active-context:exit ()
  "Exit from active context and restore normal keybindings."
  (interactive)
  (when lse-tpu:eu:active-context
    (message "Exiting active context of command %s"
      (get lse-tpu:eu:active-context 'lse:map-name)
    )
    (lse-tpu:eu:active-context:kill)
  )
; lse-tpu:eu:active-context:exit
)

(defun lse-tpu:eu:active-context:exit+delegate ()
  "Exit from active context and delegate key-binding to global keymap."
  (interactive)
  (lse-tpu:eu:active-context--delegate
    (lambda (event cmd)
      (message "Exiting active context, delegating %s to %s" event cmd)
      (lse-tpu:eu:active-context:exit)
    )
  )
; lse-tpu:eu:active-context:exit+delegate
)

;;; 18-Jan-2022
(defun lse-tpu:eu:active-context:kill ()
  "Kill active context of currently active top-lelel edit-unit command.

This can be used to clean up after an error.
  "
  (interactive)
  (when lse-tpu:eu:active-context
    (let ((map-name  (get lse-tpu:eu:active-context 'lse:map-name))
          (start-pos (get lse-tpu:eu:active-context 'lse:pos:start))
         )
      (remove-hook 'post-command-hook 'lse-tpu:eu:active-context:update)
      (lse-tpu:eu:active-context:update)
      (setq lse-tpu:edit-unit:mode-line "")
      (setq lse-tpu:eu:active-context nil)
      (when (and
              (marker-position start-pos)
              (/= (point-marker) start-pos)
            )
        (lse-tpu:saved-positions:add! start-pos)
      )
      (when (overlayp lse-tpu:eu:active-context:overlay)
        (delete-overlay lse-tpu:eu:active-context:overlay)
      )
      (when (overlayp lse-tpu:eu:active-context:curr-overlay)
        (delete-overlay lse-tpu:eu:active-context:curr-overlay)
      )
      (when (overlayp lse-tpu:eu:active-context:head-overlay)
        (delete-overlay lse-tpu:eu:active-context:head-overlay)
      )
      (when (overlayp lse-tpu:eu:active-context:region-overlay)
        (delete-overlay lse-tpu:eu:active-context:region-overlay)
      )
      (lse-pos-ring:clear lse-tpu:eu:saved-positions)
    )
  )
; lse-tpu:eu:active-context:kill
)

;;; 18-Jan-2022
(defun lse-tpu:eu:active-context:update ()
  "Update mark after command completed"
  (when lse-tpu:eu:active-context
    (when (overlayp lse-tpu:eu:active-context:curr-overlay)
      (move-overlay lse-tpu:eu:active-context:curr-overlay
        (get lse-tpu:eu:active-context 'lse:pos:bound) (point-marker)
      )
    )
    (when (get lse-tpu:eu:active-context 'lse:shifted)
      (let* ((mrk-pos   (get lse-tpu:eu:active-context 'lse:pos:mark))
             (mrk-pos-x (or mrk-pos (lse-tpu:mark) (point-marker)))
            )
        (if mrk-pos
            (lse-tpu:set-mark mrk-pos)
          (put lse-tpu:eu:active-context 'lse:pos:mark mrk-pos-x)
        )
      )
    )
    (let ((mrk-pos (get lse-tpu:eu:active-context 'lse:pos:mark))
          (pos     (point-marker))
         )
      (if mrk-pos
          (progn
            (if lse-tpu:eu:active-context:region-overlay
                (move-overlay lse-tpu:eu:active-context:region-overlay
                  mrk-pos pos
                )
              (setq lse-tpu:eu:active-context:region-overlay
                (make-overlay mrk-pos pos)
              )
              (overlay-put lse-tpu:eu:active-context:region-overlay
                'face 'region
              )
              (overlay-put lse-tpu:eu:active-context:region-overlay
                'priority 1
              )
            )
          )
        (when (overlayp lse-tpu:eu:active-context:region-overlay)
          (delete-overlay lse-tpu:eu:active-context:region-overlay)
          (setq lse-tpu:eu:active-context:region-overlay nil)
        )
      )
    )
  )
; lse-tpu:eu:active-context:update
)

;;; 12-Feb-2022
(defun lse-tpu:eu:active-region ()
  "Return active region.

If an edit-unit is active, return the selection or region of that context,
otherwise return the selection, if any."
  (let* ((head
           (if lse-tpu:eu:active-context
               (or
                 (get lse-tpu:eu:active-context 'lse:pos:mark)
                 (get lse-tpu:eu:active-context 'lse:pos:bound)
               )
             (lse-tpu:mark)
           )
         )
         (tail (point))
        )
    (when head
      (list head tail
        (if (< head tail)
            lse-tpu:direction-forward
          lse-tpu:direction-backward
        )
      )
    )
  )
; lse-tpu:eu:active-region
)

;;; 26-Jan-2022
(defun lse-tpu:eu:repeat (&rest args)
  "Repeat last command."
  (interactive "^P")
  (let ((last-cmd (get lse-tpu:eu:active-context 'lse:last-cmd))
       )
    (when (fboundp last-cmd)
      (setq this-command last-cmd)
      (apply last-cmd args)
    )
  )
; lse-tpu:eu:repeat
)

;;; 26-Jan-2022
(defun lse-tpu:eu:select ()
  "Select region from start of active context to current position."
  (interactive)
  (when lse-tpu:eu:active-context
    (lse-tpu:select t)
    (lse-put-sym-props lse-tpu:eu:active-context
      'lse:pos:mark (point-marker)
      'lse:shifted  t
    )
  )
; lse-tpu:eu:select
)

;;; 14-Feb-2022
(defun lse-tpu:eu:swap-selection ()
  "Swap current point and mark/start-pos positions."
  (interactive)
  (when lse-tpu:eu:active-context
    (let* ((mark  (get lse-tpu:eu:active-context 'lse:pos:mark))
           (bound (get lse-tpu:eu:active-context 'lse:pos:bound))
           (point (point))
          )
      (if mark
          (lse-put-sym-props lse-tpu:eu:active-context 'lse:pos:mark point)
        (lse-put-sym-props lse-tpu:eu:active-context 'lse:pos:bound point)
      )
      (goto-char (or mark bound))
    )
  )
; lse-tpu:eu:swap-selection
)

;;; 26-Jan-2022
(defun lse-tpu:eu:unselect ()
  "Un-select the region from start of active context to current position."
  (interactive)
  (when lse-tpu:eu:active-context
    (lse-tpu:unselect t)
    (lse-put-sym-props lse-tpu:eu:active-context
      'lse:pos:mark nil
      'lse:shifted  nil
    )
  )
; lse-tpu:eu:unselect
)

;;;+
;;; Cut/copy/paste commands
;;;-
;;; 12-Feb-2022
(defun lse-tpu-eu--ccp-unit-sym (unit-name)
  "Return the ccp-symbol for `unit-name`."
  (car (read-from-string (format ":%s" unit-name)))
; lse-tpu-eu--ccp-unit-sym
)

;;; 12-Feb-2022
(defun lse-tpu-eu--ccp-unit-buffer-index (unit &optional key)
  "Return index of buffer referred to by `key` of `unit`
(default for `key`: lse-tpu:cmd-char)."
  (let* ((k
           (if key
               (if (characterp key) (char-to-string key) key)
             (lse-tpu:cmd-char-as-string)
           )
         )
         (keys  (lse-assoc-value unit lse-tpu-eu--ccp-unit-keys))
         (index (string-match (regexp-quote k) keys))
        )
    (if index (1+ index) 0)
  )
; lse-tpu-eu--ccp-unit-buffer-index
)

;;; 12-Feb-2022
(defun lse-tpu-eu--ccp-unit-buffer (unit &optional key)
  "Return buffer referred to by `key` (default: lse-tpu:cmd-char) of `unit`."
  (let ((lse-tpu:ccp-buffer-index (lse-tpu-eu--ccp-unit-buffer-index unit key))
       )
    (lse-tpu:ccp-buffer:active (lse-tpu-eu--ccp-unit-sym unit))
  )
; lse-tpu-eu--ccp-unit-buffer
)

;;; 19-Feb-2022
(defun lse-tpu-eu--ccp-unit-key-hash ()
  "Return lse-tpu-eu--ccp-unit-key-hash after calculating it, if necessary."
  (let ((result lse-tpu-eu--ccp-unit-key-hash)
        key value b-index ccp-buffer
       )
    (unless result
      (setq lse-tpu-eu--ccp-unit-key-hash (make-hash-table :test 'equal))
      (setq result lse-tpu-eu--ccp-unit-key-hash)
      (pcase-dolist (`(,unit ,keys) lse-tpu-eu--ccp-unit-keys)
        (dolist (key (split-string keys "" t))
          (setq value (gethash key result nil))
          (if value
              (message "Duplicate key %s for ccp-units %s and %s"
                key unit (car value)
              )
            (setq b-index    (lse-tpu-eu--ccp-unit-buffer-index unit key))
            (setq ccp-buffer (lse-tpu-eu--ccp-unit-buffer       unit key))
            (puthash
              key
              (list unit b-index (lse-tpu:prefix-to-name b-index) ccp-buffer)
              result
            )
          )
        )
      )
    )
    result
  )
; lse-tpu-eu--ccp-unit-key-hash
)

;;; 19-Feb-2022
(defun lse-tpu-eu--ccp-completions (ukh)
  (let ((result ())
        unit b-index b-name ccp-buffer
       )
    (maphash
      (lambda (k v)
        (pcase v
          (`(,unit ,b-index ,b-name ,ccp-buffer)
            (push
              (cons k
                (format ": %6s buffer-%s <-> %s"
                  unit b-name (lse-tpu:ccp-buffer:text ccp-buffer)
                )
              )
              result
            )
          )
        )
      )
      ukh
    )
    (nreverse result)
  )
; lse-tpu-eu--ccp-completions
)

;;; 20-Feb-2022
(defun lse-tpu-eu--ccp-define-aliases (cmd &rest args)
  "Define unit/buffer specific aliases for `cmd`, adding `args` as properties."
  (save-match-data
    (let* ((name      (symbol-name cmd))
           (match     (string-match lse-tpu:eu:ccp-cmd:regexp name))
           (cmd-cat   (match-string 1 name))
           (prefix    (lse-assoc-value cmd-cat lse-tpu:eu:ccp-prefix-keys))
           (ukh       (lse-tpu-eu--ccp-unit-key-hash))
           alias cmd-name
           unit b-index b-name ccpb
          )
      (maphash
        (lambda (k v)
          (pcase v
            (`(,unit ,b-index ,b-name ,ccpb)
              (setq cmd-name (concat cmd-cat ":" unit))
              (setq alias
                (lse-define-computed-alias
                  (format "lse-tpu:eu:%s:%s:buffer-%s" cmd-cat unit b-name)
                  cmd
                  (format "%s active region to %s buffer-%s"
                    (capitalize cmd-cat) unit b-name
                  )
                )
              )
              (when args
                (apply 'lse-put-sym-props alias args)
              )
              (lse-put-sym-props alias
                'lse:cmd-cat     cmd-cat
                'lse:cmd-name    cmd-name
                'lse:keys        (vector (vector (string-to-char k)))
                'lse:prefix      prefix
                'lse:unit-name   unit
              )
              (add-to-list 'lse-tpu:eu:ccp-cmd-list alias)
              (when (string-equal cmd-cat "paste")
                (add-to-list 'lse-tpu:eu:paste-cmd-list alias)
              )
            )
          )
        )
        ukh
      )
    )
  )
; lse-tpu-eu--ccp-define-aliases
)

;;; 19-Feb-2022
(defun lse-tpu-eu--ccp-unit-completion ()
  (let* ((lse-completion:desc-indent 1)
         (lse-completion:hide-leader t)
         (lse-completion:left_margin 0)
         (ukh         (lse-tpu-eu--ccp-unit-key-hash))
         (completions (lse-tpu-eu--ccp-completions ukh))
         (key         (lse-complete "" completions t nil t nil t))
        )
    key
  )
; lse-tpu-eu--ccp-unit-completion
)

;;; 20-Feb-2022
(defun lse-tpu-eu--ccp-do-unit (fct append key)
  (let ((ukh (lse-tpu-eu--ccp-unit-key-hash))
        (k
          (if key
              (if (characterp key) (char-to-string key) key)
            (lse-tpu:cmd-char-as-string)
          )
        )
        unit b-index b-name ccpb
       )
    (pcase (gethash k ukh)
      (`(,unit ,b-index ,b-name ,ccpb)
        (funcall fct
          unit b-index b-name ccpb append k
        )
      )
    )
  )
; lse-tpu-eu--ccp-do-unit
)

;;;  5-Jun-2022
(defun lse-tpu-eu--ccp-do-paste (unit b-index b-name ccpb count key)
  (let* ((undel-fct (lse-assoc-value unit lse-tpu-eu--ccp-undelete-fct-map))
         (lse-tpu:ccp-buffer-index b-index)
        )
    (funcall undel-fct count)
  )
; lse-tpu-eu--ccp-do-paste
)

;;; 20-Feb-2022
(defun lse-tpu-eu--cut/copy-unit (ccp-fct msg-head append key)
  (lse-tpu-eu--ccp-do-unit
    (lambda (unit b-index b-name ccpb append key)
      (let (head tail dir text)
        (pcase (lse-tpu:eu:active-region)
          (`(,head ,tail ,dir)
            (when (/= head tail)
              (setq text
                (funcall ccp-fct
                  unit b-index b-name ccpb append key head tail dir
                )
              )
              (message "%s%s to %s-buffer %s%s"
                msg-head (if append "/appended" "") unit b-name
                (if text
                    (format " %s %s···"
                      (if (equal dir lse-tpu:direction-forward) "->" "<-")
                      (substring-no-properties text 0 (min 30 (length text)))
                    )
                  ""
                )
              )
            )
          )
          (_
            (message "No selection is active.")
          )
        )
      )
    )
    append key
  )
; lse-tpu-eu--cut/copy-unit
)

;;; 20-Feb-2022
(defun lse-tpu:eu:copy:unit (&optional append key)
  "Copy active region to unit's buffer, index by last key of command."
  (interactive "P")
  (lse-tpu-eu--cut/copy-unit
    (lambda (unit b-index b-name ccpb append key head tail dir)
      "Copy active region to unit's buffer"
      (let ((text (buffer-substring-no-properties head tail))
           )
        (lse-tpu:ccp-buffer:update ccpb text dir append)
        (when lse-tpu:eu:active-context
          (call-interactively 'lse-tpu:eu:active-context:exit)
        )
        text
      )
    )
    "Copied" append key
  )
; lse-tpu:eu:copy:unit
)
(lse-tpu-eu--ccp-define-aliases 'lse-tpu:eu:copy:unit)

;;; 20-Feb-2022
(defun lse-tpu:eu:cut:unit (&optional append key)
  "Cut active region to unit's buffer, indexed by last key of command."
  (interactive "P")
  (lse-tpu-eu--cut/copy-unit
    (lambda (unit b-index b-name ccpb append key head tail dir)
      "Cut active region to unit's buffer"
      (let ((text    (buffer-substring-no-properties head tail))
            (del-fct (lse-assoc-value unit lse-tpu-eu--ccp-delete-fct-map))
            (lse-tpu:ccp-buffer-index b-index)
           )
        (funcall del-fct
          head tail dir append
        )
        (when lse-tpu:eu:active-context
          (call-interactively 'lse-tpu:eu:active-context:exit)
        )
        text
      )
    )
    "Cut" append key
  )
; lse-tpu:eu:cut:unit
)
(lse-tpu-eu--ccp-define-aliases 'lse-tpu:eu:cut:unit)

;;; 20-Feb-2022
(defun lse-tpu:eu:paste:unit (&optional count key)
  "Paste active region to unit's buffer, indexed by last key of command."
  (interactive "*P")
  (lse-tpu-eu--ccp-do-unit 'lse-tpu-eu--ccp-do-paste (or count 1) key)
; lse-tpu:eu:paste:unit
)
(lse-tpu-eu--ccp-define-aliases 'lse-tpu:eu:paste:unit)

;;; 19-Feb-2022
(defun lse-tpu:eu:copy:unit/complete (&optional append)
  "Copy active region to unit buffer selected by completion."
  (interactive "P")
  (lse-tpu:eu:copy:unit append (lse-tpu-eu--ccp-unit-completion))
; lse-tpu:eu:copy:unit/complete
)
(lse-tpu:eu:add-ccp-completion-props 'lse-tpu:eu:copy:unit/complete)

;;; 19-Feb-2022
(defun lse-tpu:eu:cut:unit/complete (&optional append)
  "Cut active region to unit buffer selected by completion."
  (interactive "P")
  (lse-tpu:eu:cut:unit append (lse-tpu-eu--ccp-unit-completion))
; lse-tpu:eu:cut:unit/complete
)
(lse-tpu:eu:add-ccp-completion-props 'lse-tpu:eu:cut:unit/complete)

;;; 19-Feb-2022
(defun lse-tpu:eu:paste:unit/complete (&optional count)
  "Paste active region to unit buffer selected by completion."
  (interactive "*P")
  (lse-tpu:eu:paste:unit count (lse-tpu-eu--ccp-unit-completion))
; lse-tpu:eu:paste:unit/complete
)
(lse-tpu:eu:add-ccp-completion-props 'lse-tpu:eu:paste:unit/complete)


;;;+
;;; Top-level goto edit-unit commands
;;;-
;;;  1-Feb-2022
(defun lse-tpu:eu:goto-next:head (&optional num)
  "Switch to mode: goto beginning of next edit-unit(s)."
  (interactive "^P")
  (lse-tpu:eu:active-context:init num)
; lse-tpu:eu:goto-next:head
)
(lse-tpu:eu:add-tlg-props 'lse-tpu:eu:goto-next:head
  'lse:keymap lse-tpu:eu:keymap:goto-next:head
)

;;;  1-Feb-2022
(defun lse-tpu:eu:goto-next:tail (&optional num)
  "Switch to mode: goto end of next edit-unit(s)."
  (interactive "^P")
  (lse-tpu:eu:active-context:init num)
; lse-tpu:eu:goto-next:tail
)
(lse-tpu:eu:add-tlg-props 'lse-tpu:eu:goto-next:tail
  'lse:keymap lse-tpu:eu:keymap:goto-next:tail
)

;;;  1-Feb-2022
(defun lse-tpu:eu:goto-prev:head (&optional num)
  "Switch to mode: goto beginning of previous edit-unit(s)."
  (interactive "^P")
  (lse-tpu:eu:active-context:init num)
; lse-tpu:eu:goto-prev:head
)
(lse-tpu:eu:add-tlg-props 'lse-tpu:eu:goto-prev:head
  'lse:keymap lse-tpu:eu:keymap:goto-prev:head
)

;;;  1-Feb-2022
(defun lse-tpu:eu:goto-prev:tail (&optional num)
  "Switch to mode: goto end of previous edit-unit(s)."
  (interactive "^P")
  (lse-tpu:eu:active-context:init num)
; lse-tpu:eu:goto-prev:tail
)
(lse-tpu:eu:add-tlg-props 'lse-tpu:eu:goto-prev:tail
  'lse:keymap lse-tpu:eu:keymap:goto-prev:tail
)

;;; 18-Jan-2022
(defun lse-tpu:eu:goto:wrapper (body num &rest args)
  "Context wrapper for functions going to next/prev edit-unit head/tail."
  (lse-tpu:eu:active-context:set-cmd this-command num)
  (condition-case nil
      (let ((num (or num (get lse-tpu:eu:active-context 'lse:num))))
        (lse-pos-ring:add lse-tpu:eu:saved-positions)
        (apply body num args)
      )
    (error nil)
  )
; lse-tpu:eu:goto:wrapper
)

;;;+
;;; cat-diff (char category diff: lower case, upper case, numeric, other)
;;;-
;;;  7-Feb-2022
(defun lse-tpu:eu:cat-diff (look-fct)
  (let ((case-fold-search nil))
    (cond
      ((funcall look-fct "[a-z]")  "a-z")
      ((funcall look-fct "[A-Z]")  "A-Z")
      ((funcall look-fct "[0-9.]") "0-9.")
      (t                           "^a-zA-Z0-9.")
    )
  )
; lse-tpu:eu:cat-diff
)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-next:head:cat-diff (&optional num)
  (let ((cat-pat (lse-tpu:eu:cat-diff 'looking-at))
       )
    (skip-chars-forward cat-pat)
  )
; lse-tpu-eu--goto-next:head:cat-diff
)

(defun lse-tpu:eu:goto-next:head:cat-diff (&optional num)
  "Goto next beginning of different character category.

Categories are: lower case, upper case, numeric, other."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:head:cat-diff num)
; lse-tpu:eu:goto-next:head:cat-diff
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:head:cat-diff)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-next:tail:cat-diff (&optional num)
  (when (not
          (equal
            (lse-tpu:eu:cat-diff 'looking-at)
            (save-excursion
              (lse-tpu:forward-char)
              (lse-tpu:eu:cat-diff 'looking-at)
            )
          )
        )
    (lse-tpu:forward-char)
  )
  (let ((result (lse-tpu-eu--goto-next:head:cat-diff))
       )
    (unless (= result 0)
      (lse-tpu:backward-char 1)
    )
  )
; lse-tpu-eu--goto-next:tail:cat-diff
)

(defun lse-tpu:eu:goto-next:tail:cat-diff (&optional num)
  "Goto end of current character category.

Categories are: lower case, upper case, numeric, other."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:tail:cat-diff num)
; lse-tpu:eu:goto-next:tail:cat-diff
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:tail:cat-diff)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-prev:head:cat-diff (&optional num)
  (let ((cat-pat
          (lse-tpu:eu:cat-diff (lambda (pat) (looking-behind-at pat 1))))
       )
    (skip-chars-backward cat-pat)
  )
; lse-tpu-eu--goto-prev:head:cat-diff
)

(defun lse-tpu:eu:goto-prev:head:cat-diff (&optional num)
  "Goto beginning of current character category.

Categories are: lower case, upper case, numeric, other."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:head:cat-diff num)
; lse-tpu:eu:goto-prev:head:cat-diff
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:head:cat-diff)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-prev:tail:cat-diff (&optional num)
  (let ((result (lse-tpu-eu--goto-prev:head:cat-diff))
       )
    (unless (= result 0)
      (lse-tpu:backward-char 1)
    )
  )
; lse-tpu:eu:goto-prev:tail:cat-diff
)

(defun lse-tpu:eu:goto-prev:tail:cat-diff (&optional num)
  "Goto previous end of different character category.

Categories are: lower case, upper case, numeric, other."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:tail:cat-diff num)
; lse-tpu:eu:goto-prev:tail:cat-diff
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:tail:cat-diff)

;;;+
;;; char-oc (character occurence)
;;;-
;;;  2-Feb-2022
(defun lse-tpu-eu--goto-next:head:char-oc (num)
  (lse-tpu:goto-next-char num nil (lse-tpu:cmd-key))
; lse-tpu-eu--goto-next:head:char-oc
)

(defun lse-tpu:eu:goto-next:head:char-oc (&optional num)
  "Goto beginning of next occurence of character.

The character is specified by the key event."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:head:char-oc num)
; lse-tpu:eu:goto-next:head:char-oc
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:head:char-oc)

(defun lse-tpu-eu--goto-next:tail:char-oc (num)
  (lse-tpu:goto-next-char num nil (lse-tpu:cmd-key))
  (lse-tpu:forward-char)
; lse-tpu-eu--goto-next:tail:char-oc
)

(defun lse-tpu:eu:goto-next:tail:char-oc (&optional num)
  "Goto end of next occurence of character.

The character is specified by the key event."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:tail:char-oc num)
; lse-tpu:eu:goto-next:tail:char-oc
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:tail:char-oc)

(defun lse-tpu-eu--goto-prev:head:char-oc (num)
  (lse-tpu:goto-prev-char num nil (lse-tpu:cmd-key))
; lse-tpu-eu--goto-prev:head:char-oc
)

(defun lse-tpu:eu:goto-prev:head:char-oc (&optional num)
  "Goto beginning of previous occurence of character.

The character is specified by the key event."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:head:char-oc num)
; lse-tpu:eu:goto-prev:head:char-oc
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:head:char-oc)

(defun lse-tpu-eu--goto-prev:tail:char-oc (num)
  (lse-tpu:goto-prev-char num nil (lse-tpu:cmd-key))
  (lse-tpu:backward-char)
; lse-tpu-eu--goto-prev:tail:char-oc
)

(defun lse-tpu:eu:goto-prev:tail:char-oc (&optional num)
  "Goto end of previous occurence of character.

The character is specified by the key event."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:tail:char-oc num)
; lse-tpu:eu:goto-prev:tail:char-oc
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:tail:char-oc)

;;;+
;;; defuns
;;;-
;;;  7-Feb-2022
(defun lse-tpu-eu--goto-next:head:defun (num)
  (beginning-of-defun (- (or num 1)))
; lse-tpu-eu--goto-next:head:defun
)

(defun lse-tpu:eu:goto-next:head:defun (&optional num)
  "Goto beginning of next defun."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:head:defun num)
; lse-tpu:eu:goto-next:head:defun
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:head:defun)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-next:tail:defun (num)
  (lse-tpu:forward-char)
  (end-of-defun num)
  (lse-tpu:backward-char)
; lse-tpu-eu--goto-next:tail:defun
)

(defun lse-tpu:eu:goto-next:tail:defun (&optional num)
  "Goto next end of defun."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:tail:defun num)
; lse-tpu:eu:goto-next:tail:defun
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:tail:defun)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-prev:head:defun (num)
  (beginning-of-defun num)
; lse-tpu-eu--goto-prev:head:defun
)

(defun lse-tpu:eu:goto-prev:head:defun (&optional num)
  "Goto previous beginning of defun."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:head:defun num)
; lse-tpu:eu:goto-prev:head:defun
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:head:defun)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-prev:tail:defun (num)
  (end-of-defun (- (or num 1)))
  (lse-tpu:backward-char)
; lse-tpu-eu--goto-prev:tail:defun
)

(defun lse-tpu:eu:goto-prev:tail:defun (&optional num)
  "Goto end of previous defun."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:tail:defun num)
; lse-tpu:eu:goto-prev:tail:defun
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:tail:defun)

;;;+
;;; lines
;;;-
;;; 18-Jan-2022
(defun lse-tpu-eu--goto-next:head:line (num)
  (lse-tpu:forward-line num)
; lse-tpu-eu--goto-next:head:line
)

(defun lse-tpu:eu:goto-next:head:line (&optional num)
  "Goto beginning of next line."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:head:line num)
; lse-tpu:eu:goto-next:head:line
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:head:line)

;;; 18-Jan-2022
(defun lse-tpu-eu--goto-next:tail:line (num)
  (lse-tpu:next-end-of-line num)
; lse-tpu-eu--goto-next:tail:line
)

(defun lse-tpu:eu:goto-next:tail:line (&optional num)
  "Goto end of next line."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:tail:line num)
; lse-tpu:eu:goto-next:tail:line
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:tail:line)

;;; 18-Jan-2022
(defun lse-tpu-eu--goto-prev:head:line (num)
  (lse-tpu:next-beginning-of-line num)
; lse-tpu-eu--goto-prev:head:line
)

(defun lse-tpu:eu:goto-prev:head:line (&optional num)
  "Goto beginning of previous line."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:head:line num)
; lse-tpu:eu:goto-prev:head:line
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:head:line)

;;; 18-Jan-2022
(defun lse-tpu-eu--goto-prev:tail:line (num)
  (lse-tpu:next-end-of-line (- num))
; lse-tpu-eu--goto-prev:tail:line
)

(defun lse-tpu:eu:goto-prev:tail:line (&optional num)
  "Goto end of previous line."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:tail:line num)
; lse-tpu:eu:goto-prev:tail:line
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:tail:line)

;;;+
;;; lists: `list`, `down-list`, `up-list`
;;;-
;;;  7-Feb-2022
(defun lse-tpu-eu--goto-next:head:down-list (&optional num)
  (condition-case nil
      (down-list)
    (error (message "Already at bottom") num)
  )
; lse-tpu-eu--goto-next:head:down-list
)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-next:tail:down-list (&optional num)
  (condition-case nil
      (progn
        (down-list num)
        (up-list)
        (lse-tpu:backward-char)
      )
    (error (message "Already at bottom"))
  )
; lse-tpu-eu--goto-next:tail:down-list
)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-prev:head:down-list (&optional num)
  (condition-case nil
      (backward-list num)
    (scan-error
      (condition-case nil
          (backward-up-list)
          (down-list)
        (error nil)
      )
    )
  )
; lse-tpu-eu--goto-prev:head:down-list
)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-prev:tail:down-list (&optional num)
  (condition-case nil
      (condition-case nil
          (backward-list num)
        (scan-error
          (backward-up-list)
        )
      )
  )
  (condition-case nil
      (progn
        (forward-list)
        (lse-tpu:backward-char)
      )
    (error nil
    )
  )
; lse-tpu-eu--goto-prev:tail:down-list
)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-next:head:list (&optional num)
  (condition-case nil
      (let ((bol (looking-at "[[({]"))
           )
        (forward-list num)
        (when bol (forward-list))
        (backward-list)
      )
    (scan-error
      (up-list)
      (lse-tpu-eu--goto-next:head:list num)
    )
    (error nil)
  )
; lse-tpu-eu--goto-next:head:list
)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-next:tail:list (&optional num)
  (condition-case nil
      (let ((bol (looking-at "\("))
           )
        (forward-list num)
      )
    (scan-error
      (up-list)
      (lse-tpu-eu--goto-next:tail:list num)
    )
    (error nil)
  )
; lse-tpu-eu--goto-next:tail:list
)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-prev:head:list (&optional num)
  (condition-case nil
      (backward-list num)
    (scan-error
      (backward-up-list num)
    )
    (error nil)
  )
; lse-tpu-eu--goto-prev:head:list
)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-prev:tail:list (&optional num)
  (condition-case nil
      (let ((eol (looking-behind-at "[])}]" 1))
           )
        (when eol (backward-list))
        (backward-list num)
        (forward-list)
      )
    (scan-error
      (backward-up-list)
    )
    (error nil)
  )
; lse-tpu-eu--goto-prev:tail:list
)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-next:head:up-list (&optional num)
  (condition-case nil
      (progn
        (up-list num)
        (lse-tpu-eu--goto-next:head:list)
      )
    (scan-error nil)
  )
; lse-tpu-eu--goto-next:head:up-list
)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-next:tail:up-list (&optional num)
  (condition-case nil
      (up-list num)
    (scan-error (message "Already at top level."))
  )
; lse-tpu-eu--goto-next:tail:up-list
)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-prev:head:up-list (&optional num)
  (condition-case nil
      (backward-up-list num)
    (scan-error (message "Already at top-level."))
  )
; lse-tpu-eu--goto-prev:head:up-list
)

;;;  7-Feb-2022
(defun lse-tpu-eu--goto-prev:tail:up-list (&optional num)
  (condition-case nil
      (progn
        (backward-up-list num)
        (lse-tpu-eu--goto-prev:tail:list)
      )
    (scan-error (message "Already at top level."))
  )
; lse-tpu-eu--goto-prev:tail:up-list
)

;;;  7-Feb-2022
(defun lse-tpu:eu:goto-next:head:down-list (&optional num)
  "Goto beginning of next list one level down."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:head:down-list num)
; lse-tpu:eu:goto-next:head:down-list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:head:down-list)

;;;  7-Feb-2022
(defun lse-tpu:eu:goto-next:tail:down-list (&optional num)
  "Goto beginning of next list one level down."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:tail:down-list num)
; lse-tpu:eu:goto-next:tail:down-list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:tail:down-list)

;;;  7-Feb-2022
(defun lse-tpu:eu:goto-prev:head:down-list (&optional num)
  "Goto beginning of previous list one level down."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:head:down-list num)
; lse-tpu:eu:goto-prev:head:down-list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:head:down-list)

;;;  7-Feb-2022
(defun lse-tpu:eu:goto-prev:tail:down-list (&optional num)
  "Goto end of previous list one level down."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:tail:down-list num)
; lse-tpu:eu:goto-prev:tail:down-list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:tail:down-list)

;;;  7-Feb-2022
(defun lse-tpu:eu:goto-next:head:list (&optional num)
  "Goto beginning of next list."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:head:list num)
; lse-tpu:eu:goto-next:head:list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:head:list)

;;;  7-Feb-2022
(defun lse-tpu:eu:goto-next:tail:list (&optional num)
  "Goto next end of list."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:tail:list num)
; lse-tpu:eu:goto-next:tail:list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:tail:list)

;;;  7-Feb-2022
(defun lse-tpu:eu:goto-prev:head:list (&optional num)
  "Goto previous beginning of list."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:head:list num)
; lse-tpu:eu:goto-prev:head:list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:head:list)

;;;  7-Feb-2022
(defun lse-tpu:eu:goto-prev:tail:list (&optional num)
  "Goto previous end of list."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:tail:list num)
; lse-tpu:eu:goto-prev:tail:list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:tail:list)

;;;  7-Feb-2022
(defun lse-tpu:eu:goto-next:head:up-list (&optional num)
  "Goto beginning of next list one level up."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:head:up-list num)
; lse-tpu:eu:goto-next:head:up-list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:head:up-list)

;;;  7-Feb-2022
(defun lse-tpu:eu:goto-next:tail:up-list (&optional num)
  "Goto end of next list one level up."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:tail:up-list num)
; lse-tpu:eu:goto-next:tail:up-list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:tail:up-list)

;;;  7-Feb-2022
(defun lse-tpu:eu:goto-prev:head:up-list (&optional num)
  "Goto previous beginning of list one level up."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:head:up-list num)
; lse-tpu:eu:goto-prev:head:up-list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:head:up-list)

;;;  7-Feb-2022
(defun lse-tpu:eu:goto-prev:tail:up-list (&optional num)
  "Goto end of previous list one level up."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:tail:up-list num)
; lse-tpu:eu:goto-prev:tail:up-list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:tail:up-list)

;;;+
;;; pages
;;;-
;;;  8-Feb-2022
(defun lse-tpu-eu--goto-next:head:page (num)
  (lse-tpu:page-forward num)
  (skip-chars-forward " \t\n\f")
; lse-tpu-eu--goto-next:head:page
)

(defun lse-tpu:eu:goto-next:head:page (&optional num)
  "Goto beginning of next page."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:head:page num)
; lse-tpu:eu:goto-next:head:page
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:head:page)

;;;  8-Feb-2022
(defun lse-tpu-eu--goto-next:tail:page (num)
  (skip-chars-forward " \t\n\f")
  (lse-tpu:page-forward num)
  (skip-chars-backward " \t\n\f")
; lse-tpu-eu--goto-next:tail:page
)

(defun lse-tpu:eu:goto-next:tail:page (&optional num)
  "Goto next end of page."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:tail:page num)
; lse-tpu:eu:goto-next:tail:page
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:tail:page)

;;;  8-Feb-2022
(defun lse-tpu-eu--goto-prev:head:page (num)
  (skip-chars-backward " \t\n\f")
  (lse-tpu:page-backward num)
  (skip-chars-forward " \t\n\f")
; lse-tpu-eu--goto-prev:head:page
)

(defun lse-tpu:eu:goto-prev:head:page (&optional num)
  "Goto previous beginning of page."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:head:page num)
; lse-tpu:eu:goto-prev:head:page
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:head:page)


;;;  8-Feb-2022
(defun lse-tpu-eu--goto-prev:tail:page (num)
  (lse-tpu:page-backward num)
  (skip-chars-backward " \t\n\f")
; lse-tpu-eu--goto-prev:tail:page
)

(defun lse-tpu:eu:goto-prev:tail:page (&optional num)
  "Goto previous end of page."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:tail:page num)
; lse-tpu:eu:goto-prev:tail:page
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:tail:page)

;;;+
;;; paragraphs
;;;-
;;;  8-Feb-2022
(defun lse-tpu-eu--goto-next:head:paragraph (num)
  (forward-paragraph num)
  (skip-chars-forward " \t\n\f")
; lse-tpu-eu--goto-next:head:paragraph
)

(defun lse-tpu:eu:goto-next:head:paragraph (&optional num)
  "Goto beginning of next paragraph."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:head:paragraph num)
; lse-tpu:eu:goto-next:head:paragraph
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:head:paragraph)

;;;  8-Feb-2022
(defun lse-tpu-eu--goto-next:tail:paragraph (num)
  (skip-chars-forward " \t\n\f")
  (forward-paragraph num)
  (skip-chars-backward " \t\n\f")
; lse-tpu-eu--goto-next:tail:paragraph
)

(defun lse-tpu:eu:goto-next:tail:paragraph (&optional num)
  "Goto next end of paragraph."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:tail:paragraph num)
; lse-tpu:eu:goto-next:tail:paragraph
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:tail:paragraph)

;;;  8-Feb-2022
(defun lse-tpu-eu--goto-prev:head:paragraph (num)
  (skip-chars-backward " \t\n\f")
  (backward-paragraph)
  (skip-chars-forward " \t\n\f" num)
; lse-tpu-eu--goto-prev:head:paragraph
)

(defun lse-tpu:eu:goto-prev:head:paragraph (&optional num)
  "Goto beginning of previous paragraph."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:head:paragraph num)
; lse-tpu:eu:goto-prev:head:paragraph
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:head:paragraph)

;;;  8-Feb-2022
(defun lse-tpu-eu--goto-prev:tail:paragraph (num)
  (backward-paragraph num)
  (skip-chars-backward " \t\n\f")
; lse-tpu-eu--goto-prev:tail:paragraph
)

(defun lse-tpu:eu:goto-prev:tail:paragraph (&optional num)
  "Goto end of previous paragraph."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:tail:paragraph num)
; lse-tpu:eu:goto-prev:tail:paragraph
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:tail:paragraph)

;;;+
;;; bs-words (blank separated words)
;;;-
;;; 17-Jan-2022
(defun lse-tpu-eu--goto-next:head:bs-word (num)
  (lse-tpu:goto-next-bs-word-head num)
; lse-tpu-eu--goto-next:head:bs-word
)

(defun lse-tpu:eu:goto-next:head:bs-word (&optional num)
  "Goto beginning of next word (blank-separated)."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:head:bs-word num)
; lse-tpu:eu:goto-next:head:bs-word
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:head:bs-word)

(defun lse-tpu-eu--goto-next:tail:bs-word (num)
  (lse-tpu:goto-next-bs-word-tail num)
; lse-tpu-eu--goto-next:tail:bs-word
)

(defun lse-tpu:eu:goto-next:tail:bs-word (&optional num)
  "Goto end of next word (blank-separated)."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:tail:bs-word num)
; lse-tpu:eu:goto-next:tail:bs-word
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:tail:bs-word)

;;; 18-Jan-2022
(defun lse-tpu-eu--goto-prev:head:bs-word (num)
  (lse-tpu:goto-prev-bs-word-head num)
; lse-tpu-eu--goto-prev:head:bs-word
)

(defun lse-tpu:eu:goto-prev:head:bs-word (&optional num)
  "Goto beginning of previous word (blank-separated)."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:head:bs-word num)
; lse-tpu:eu:goto-prev:head:bs-word
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:head:bs-word)

;;; 18-Jan-2022
(defun lse-tpu-eu--goto-prev:tail:bs-word (num)
  (lse-tpu:goto-prev-bs-word-tail num)
; lse-tpu-eu--goto-prev:tail:bs-word
)

(defun lse-tpu:eu:goto-prev:tail:bs-word (&optional num)
  "Goto end of previous word (blank-separated)."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:tail:bs-word num)
; lse-tpu:eu:goto-prev:tail:bs-word
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:tail:bs-word)

;;;+
;;; words
;;;-
;;; 18-Jan-2022
(defun lse-tpu-eu--goto-next:head:word (num)
  (lse-tpu:goto-next-word-head num)
; lse-tpu-eu--goto-next:head:word
)

(defun lse-tpu:eu:goto-next:head:word (&optional num)
  "Goto beginning of next word."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:head:word num)
; lse-tpu:eu:goto-next:head:word
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:head:word)

;;; 18-Jan-2022
(defun lse-tpu-eu--goto-next:tail:word (num)
  (lse-tpu:goto-next-word-tail num)
; lse-tpu-eu--goto-next:tail:word
)

(defun lse-tpu:eu:goto-next:tail:word (&optional num)
  "Goto end of next word."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-next:tail:word num)
; lse-tpu:eu:goto-next:tail:word
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:tail:word)

;;; 18-Jan-2022
(defun lse-tpu-eu--goto-prev:head:word (num)
  (lse-tpu:goto-prev-word-head num)
; lse-tpu-eu--goto-prev:head:word
)

(defun lse-tpu:eu:goto-prev:head:word (&optional num)
  "Goto beginning of previous word."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:head:word num)
; lse-tpu:eu:goto-prev:head:word
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:head:word)

;;; 18-Jan-2022
(defun lse-tpu-eu--goto-prev:tail:word (num)
  (lse-tpu:goto-prev-word-tail num)
; lse-tpu-eu--goto-prev:tail:word
)

(defun lse-tpu:eu:goto-prev:tail:word (&optional num)
  "Goto end of previous word."
  (interactive "^p")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--goto-prev:tail:word num)
; lse-tpu:eu:goto-prev:tail:word
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:tail:word)

;;;+
;;; wordlets
;;; - sub-units of a word separated by any character in
;;;   lse-tpu:eu:wordlet:seps
;;;-
;;; 14-Feb-2022
(defun lse-tpu-eu--goto-wordlet (goto-fct num)
  (let* ((wl-sep lse-tpu:eu:wordlet:seps)
         (wl-pat (concat "[" wl-sep "]"))
         (lse-tpu:word-chars
           (concat
             (replace-regexp-in-string
               wl-pat "" lse-tpu:ident-group-chars
             )
             lse-tpu:ident-chars
           )
         )
        )
    (funcall goto-fct num wl-sep wl-pat)
  )
; lse-tpu-eu--goto-wordlet
)

;;; 14-Feb-2022
(defun lse-tpu-eu--goto-next:head:wordlet (num)
  (lse-tpu-eu--goto-wordlet
    (function
      (lambda (num wl-sep wl-pat)
        (lse-tpu:goto-next-word-head num)
        (skip-chars-forward wl-sep)
      )
    )
    num
  )
; lse-tpu-eu--goto-next:head:wordlet
)

;;; 17-Jan-2022
(defun lse-tpu:eu:goto-next:head:wordlet (&optional num)
  "Goto beginning of next wordlet.

Separators `lse-tpu:eu:wordlet:seps'."
  (interactive "^p")
  (lse-tpu-eu--goto-next:head:wordlet num)
; lse-tpu:eu:goto-next:head:wordlet
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:head:wordlet)

;;; 14-Feb-2022
(defun lse-tpu-eu--goto-next:tail:wordlet (num)
  (lse-tpu-eu--goto-wordlet
    (function
      (lambda (num wl-sep wl-pat)
        (when (looking-at wl-pat)
          (skip-chars-forward wl-sep)
        )
        (lse-tpu:goto-next-word-tail num)
      )
    )
    num
  )
; lse-tpu-eu--goto-next:tail:wordlet
)

;;; 17-Jan-2022
(defun lse-tpu:eu:goto-next:tail:wordlet (&optional num)
  "Goto end of next wordlet.

Separators `lse-tpu:eu:wordlet:seps'."
  (interactive "^p")
  (lse-tpu-eu--goto-next:tail:wordlet num)
; lse-tpu:eu:goto-next:tail:wordlet
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-next:tail:wordlet)

;;; 14-Feb-2022
(defun lse-tpu-eu--goto-prev:head:wordlet (num)
  (lse-tpu-eu--goto-wordlet
    (function
      (lambda (num wl-sep wl-pat)
        (when (looking-behind-at wl-pat 1)
          (skip-chars-backward wl-sep)
        )
        (lse-tpu:goto-prev-word-head num)
      )
    )
    num
  )
; lse-tpu-eu--goto-prev:head:wordlet
)

;;; 18-Jan-2022
(defun lse-tpu:eu:goto-prev:head:wordlet (&optional num)
  "Goto beginning of previous wordlet.

Separators `lse-tpu:eu:wordlet:seps'."
  (interactive "^p")
  (lse-tpu-eu--goto-prev:head:wordlet num)
; lse-tpu:eu:goto-prev:head:wordlet
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:head:wordlet)

(defun lse-tpu-eu--goto-prev:tail:wordlet (num)
  (lse-tpu-eu--goto-wordlet
    (function
      (lambda (num wl-sep wl-pat)
        (lse-tpu:goto-prev-word-tail num)
        (skip-chars-backward wl-sep)
      )
    )
    num
  )
; lse-tpu-eu--goto-prev:tail:wordlet
)

;;; 18-Jan-2022
(defun lse-tpu:eu:goto-prev:tail:wordlet (&optional num)
  "Goto end of previous wordlet.

separators `lse-tpu:eu:wordlet:seps'."
  (interactive "^P")
  (lse-tpu-eu--goto-prev:tail:wordlet num)
; lse-tpu:eu:goto-prev:tail:wordlet
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:goto-prev:tail:wordlet)

;;;+
;;; Select-unit edit-unit commands
;;;-
;;; 14-Feb-2022
;;; Top-level select-unit command
(defun lse-tpu:eu:select-unit (&optional num)
  "Switch to mode: select edit-unit."
  (interactive "^P")
  (lse-tpu:eu:active-context:init num)
; lse-tpu:eu:select-unit
)
(lse-tpu:eu:add-tlg-props 'lse-tpu:eu:select-unit
  'lse:keymap lse-tpu:eu:keymap:select-unit
)

;;; 15-Feb-2022
(defun lse-tpu-eu--select-unit:do (num first second)
  (lse-tpu:eu:unselect)
  (funcall first num)
  (lse-tpu:eu:select)
  (funcall second num)
; lse-tpu-eu--select-unit:do
)

;;; 15-Feb-2022
(defun lse-tpu-eu--select-unit:bs-word (&optional num)
  (let ((lse-tpu:word-chars lse-tpu:blank-sep-word-chars))
    (lse-tpu-eu--select-unit:word num)
  )
; lse-tpu-eu--select-unit:bs-word
)

(defun lse-tpu:eu:select-unit:bs-word (&optional num)
  "Select current bs-word."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--select-unit:bs-word num)
; lse-tpu:eu:select-unit:bs-word
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:select-unit:bs-word)

;;; 15-Feb-2022
(defun lse-tpu-eu--select-unit:cat-diff (&optional num)
  (lse-tpu-eu--select-unit:do num
    (lambda (num) (lse-tpu-eu--goto-prev:head:cat-diff num))
    (lambda (num) (lse-tpu-eu--goto-next:head:cat-diff 1))
  )
; lse-tpu-eu--select-unit:cat-diff
)

(defun lse-tpu:eu:select-unit:cat-diff (&optional num)
  "Select current cat-diff."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--select-unit:cat-diff num)
; lse-tpu:eu:select-unit:cat-diff
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:select-unit:cat-diff)

;;; 15-Feb-2022
(defun lse-tpu-eu--select-unit:char-region (&optional num)
  (let* ((no-delim (equal num '-))
         (num      (if no-delim 1 num))
        )
    (lse-tpu-eu--select-unit:do num
      (lambda (num)
        (lse-tpu-eu--goto-prev:head:char-oc num)
        (when no-delim (lse-tpu:forward-char 1))
      )
      (lambda (num)
        (lse-tpu-eu--goto-next:tail:char-oc 1)
        (when no-delim (lse-tpu:forward-char -1))
      )
    )
  )
; lse-tpu-eu--select-unit:char-region
)

;;; 15-Feb-2022
(defun lse-tpu:eu:select-unit:char-region (&optional num)
  "Select character region.

The character is specified by the key event."
  (interactive "P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--select-unit:char-region num)
; lse-tpu:eu:select-unit:char-region
)
; (lse-tpu:eu:add-goto-props 'lse-tpu:eu:select-unit:char-region)

;;; 15-Feb-2022
(defun lse-tpu-eu--select-unit:d-region (&optional num)
  ;; movement code stolen from `lse-select-bracketed-range`
  (let* ((no-delim (equal num '-))
         (next-p   (and (integerp num) (/= num 1)))
         (next-num
           (when next-p
             (if (> num 1) (1- num) num)
           )
         )
        )
    (when (if next-p
              (lse-tpu:goto-next-char next-num)
            (lse-tpu:goto-opening-char 1)
          )
      (lse-tpu:eu:unselect)
      (save-excursion
        (when no-delim (lse-tpu:forward-char 1))
        (lse-tpu:eu:select)
      )
      (forward-list 1)
      (when no-delim (lse-tpu:forward-char -1))
    )
  )
; lse-tpu-eu--select-unit:d-region
)

(defun lse-tpu:eu:select-unit:d-region (&optional num)
  "Select current delimited region.

The delimiter is specified by the key event.
A prefix argument `-` excludes the delimiters,
numeric prefix arguments select next or previous delimited regions."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--select-unit:d-region num)
; lse-tpu:eu:select-unit:d-region
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:select-unit:d-region)

;;; 14-Feb-2022
(defun lse-tpu-eu--select-unit:defun (&optional num)
  (lse-tpu-eu--select-unit:do num
    (lambda (num) (lse-tpu-eu--goto-prev:head:defun num))
    (lambda (num) (lse-tpu-eu--goto-next:tail:defun 1))
  )
; lse-tpu-eu--select-unit:defun
)

(defun lse-tpu:eu:select-unit:defun (&optional num)
  "Select current defun."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--select-unit:defun num)
; lse-tpu:eu:select-unit:defun
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:select-unit:defun)

;;; 15-Feb-2022
(defun lse-tpu-eu--select-unit:down-list (&optional num)
  (lse-tpu-eu--select-unit:do num
    (lambda (num) (lse-tpu-eu--goto-next:head:down-list num))
    (lambda (num)
      (lse-tpu-eu--goto-next:tail:up-list 1)
      (lse-tpu:backward-char 1)
    )
  )
; lse-tpu-eu--select-unit:down-list
)

(defun lse-tpu:eu:select-unit:down-list (&optional num)
  "Select current down-list."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--select-unit:down-list num)
; lse-tpu:eu:select-unit:down-list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:select-unit:down-list)

;;; 15-Feb-2022
(defun lse-tpu-eu--select-unit:line (&optional num)
  (lse-tpu-eu--select-unit:do num
    (lambda (num) (goto-char (lse-tpu:line-head-pos)))
    (lambda (num)
      (if (and (bolp) (eolp))
          (lse-tpu:forward-char 1)
        (goto-char (lse-tpu:line-tail-pos))
      )
    )
  )
; lse-tpu-eu--select-unit:line
)

(defun lse-tpu:eu:select-unit:line (&optional num)
  "Select current line."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--select-unit:line num)
; lse-tpu:eu:select-unit:line
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:select-unit:line)

;;; 15-Feb-2022
(defun lse-tpu-eu--select-unit:list (&optional num)
  (lse-tpu-eu--select-unit:do num
    (lambda (num)
      (unless (looking-at "(")
        (lse-tpu-eu--goto-prev:head:list num)
      )
    )
    (lambda (num)
      (lse-tpu-eu--goto-next:tail:list 1)
    )
  )
; lse-tpu-eu--select-unit:list
)

(defun lse-tpu:eu:select-unit:list (&optional num)
  "Select current list."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--select-unit:list num)
; lse-tpu:eu:select-unit:list
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:select-unit:list)

;;; 15-Feb-2022
(defun lse-tpu-eu--select-unit:page (&optional num)
  (lse-tpu-eu--select-unit:do num
    (lambda (num)
      (unless (looking-at "")
        (lse-tpu-eu--goto-prev:head:page num)
      )
    )
    (lambda (num)
      (lse-tpu-eu--goto-next:tail:page 1)
    )
  )
; lse-tpu-eu--select-unit:page
)

(defun lse-tpu:eu:select-unit:page (&optional num)
  "Select current page."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--select-unit:page num)
; lse-tpu:eu:select-unit:page
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:select-unit:page)

;;; 15-Feb-2022
(defun lse-tpu-eu--select-unit:paragraph (&optional num)
  (lse-tpu-eu--select-unit:do num
    (lambda (num) (lse-tpu-eu--goto-prev:head:paragraph num))
    (lambda (num) (lse-tpu-eu--goto-next:tail:paragraph 1))
  )
; lse-tpu-eu--select-unit:paragraph
)

(defun lse-tpu:eu:select-unit:paragraph (&optional num)
  "Select current paragraph."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--select-unit:paragraph num)
; lse-tpu:eu:select-unit:paragraph
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:select-unit:paragraph)

;;; 15-Feb-2022
(defun lse-tpu-eu--select-unit:word (&optional num)
  (lse-tpu-eu--select-unit:do num
    (lambda (num) (goto-char (lse-tpu:curr-word-head-pos)))
    (lambda (num) (goto-char (lse-tpu:curr-word-tail-pos)))
  )
; lse-tpu-eu--select-unit:word
)

(defun lse-tpu:eu:select-unit:word (&optional num)
  "Select current word."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--select-unit:word num)
; lse-tpu:eu:select-unit:word
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:select-unit:word)

;;; 15-Feb-2022
(defun lse-tpu-eu--select-unit:wordlet (&optional num)
  (let* ((wl-sep lse-tpu:eu:wordlet:seps)
         (wl-pat (concat "[" wl-sep "]"))
         (lse-tpu:word-chars
           (concat
             (replace-regexp-in-string
               wl-pat "" lse-tpu:ident-group-chars
             )
             lse-tpu:ident-chars
           )
         )
        )
    (lse-tpu-eu--select-unit:do num
      (lambda (num) (goto-char (lse-tpu:curr-word-head-pos)))
      (lambda (num) (goto-char (lse-tpu:curr-word-tail-pos)))
    )
  )
; lse-tpu-eu--select-unit:wordlet
)

(defun lse-tpu:eu:select-unit:wordlet (&optional num)
  "Select current wordlet."
  (interactive "^P")
  (lse-tpu:eu:goto:wrapper 'lse-tpu-eu--select-unit:wordlet num)
; lse-tpu:eu:select-unit:wordlet
)
(lse-tpu:eu:add-goto-props 'lse-tpu:eu:select-unit:wordlet)

(provide 'lse-tpu-edit-units)

;;;; __END__ lse-tpu-edit-units
