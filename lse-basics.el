;-*- coding: utf-8 -*-

;;;; Copyright (C) 1995-2022 Christian Tanzer. All rights reserved.
;;;; tanzer@gg32.com                                      https://www.gg32.com

;;;; This file is part of LS-Emacs, a package built on top of GNU Emacs.
;;;;
;;;; Like GNU Emacs, LS-Emacs is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU General Public License as published
;;;; by the Free Software Foundation; either version 2, or (at your option)
;;;; any later version.
;;;;
;;;; Like GNU Emacs, LS-Emacs is distributed in the hope that it will be
;;;; useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with GNU Emacs; see the file COPYING.  If not, write to
;;;; the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.

;;;;++
;;;; Name
;;;;    lse-basics
;;;;
;;;; Purpose
;;;;    Functions for insertion into flat fill-in's
;;;;
;;;; Revision Dates
;;;;    19-Mar-1995 (CT) Creation (of comment)
;;;;    19-Mar-1995 (CT) lse-use-lse-self-insert-command and
;;;;                     lse-use-emacs-self-insert-command defined as no-op's
;;;;     4-Oct-1996 (CT) Define buffer-substring-no-properties if not fboundp
;;;;                     (accommodate older Emacs versions)
;;;;     7-Oct-1996 (CT) Don't redefine 'insert (hooks take care of that)
;;;;     7-Oct-1996 (CT) lse-self-insert-command commented out
;;;;     7-Oct-1996 (CT) lse-insert              commented out
;;;;    13-Dec-1997 (CT) chars-in-string added for compatibility with 19.n
;;;;    29-Dec-1997 (CT) defsubst        added
;;;;    30-Dec-1997 (CT) lse-safe        added
;;;;    15-Oct-2007 (CT) Cruft removed (lse-insert, ...)
;;;;    29-Jul-2009 (CT) Modernize use of backquotes
;;;;    10-Nov-2010 (CT) `string-starts-with` and `string-ends-with` added
;;;;    26-Feb-2012 (CT) Add `string-has-upper-case-p` and `string-mixed-case-p`
;;;;    20-Jul-2012 (CT) Guard `string-*` functions with `lse-safe`
;;;;    19-Jan-2022 (CT) Add `lse-put-sym-props`
;;;;    22-Jan-2022 (CT) Add `lse-assoc-value`
;;;;    20-Feb-2022 (CT) Add `lse-define-computed-alias`
;;;;    ««revision-date»»···
;;;;--
(provide 'lse-basics)

(defvar lse::emacs-insert:replaced nil)

(defun lse-initialization ()
  (if (not lse::emacs-insert:replaced)
      (progn
        (setq lse::emacs-insert:replaced t)
        (fset 'lse-fill-in-insert   (symbol-function 'insert))
        ;;  7-Oct-1996 ;; not needed anymore ;; (fset 'insert 'lse-insert)
      )
  )
)
(lse-initialization)

(if (fboundp 'buffer-substring-no-properties)
    nil
 (fset 'buffer-substring-no-properties (symbol-function 'buffer-substring))
)

(if (fboundp 'chars-in-string)
    nil
  ;;; 13-Dec-1997
  (fset 'chars-in-string (symbol-function 'length))
)

(if (fboundp 'defsubst)
    nil
  ;;; 29-Dec-1997
  (fset 'defsubst (symbol-function 'defun))
)

;;; 22-Jan-2022
(defun lse-assoc-value (key alist &optional testfn)
  "Return the value of `key' in `alist', if any."
  (lse-safe
    (cadr (assoc key alist testfn))
  )
; lse-assoc-value
)

;;; 20-Feb-2022
(defmacro lse-define-computed-alias (name fct &optional doc)
   "Define a symbol named `name` as alias to `fct` with documentation `doc`."
   (list 'defalias (list 'intern name) fct doc)
 ; lse-define-computed-alias
)

;;; 19-Jan-2022
(defun lse-put-sym-props (sym &rest args)
  "Put symbol properties specified by `args' onto `sym's property list.
`args' can be a single property list or a number of property/value pairs.
"
  (let* ((pl-arg
           (if (equal (safe-length args) 1)
               (car args)
             args
           )
         )
         prop val
        )
    (while pl-arg
      (setq prop   (car  pl-arg))
      (setq val    (cadr pl-arg))
      (setq pl-arg (cddr pl-arg))
      (put sym prop val)
    )
  )
; lse-put-sym-props
)

;;; 30-Dec-1997 ;;; stolen from cc-defs.el
(defmacro lse-safe (&rest body)
  ;; safely execute BODY, return nil if an error occurred
  `(condition-case nil
       (progn ,@body)
     (error nil)
   )
)

;;; 10-Nov-2010
(defun string-starts-with (string starter)
  "Returns true if `string` starts with `starter`, false otherwise"
  (lse-safe
    (let ((l (length starter)))
      (string= (substring-no-properties string 0 l) starter)
    )
  )
; string-starts-with
)

;;; 10-Nov-2010
(defun string-ends-with (string ender)
  "Returns true if `string` ends with `ender`, false otherwise"
  (lse-safe
    (let ((l (length ender)))
      (string= (substring-no-properties string (- l)) ender)
    )
  )
; string-ends-with
)

;;; 26-Feb-2012
(defun string-has-upper-case-p (s)
  "Returns true if `s` contains both some upper case characters"
  (lse-safe
    (let ((case-fold-search nil)
         )
      (save-match-data
        (integerp (string-match "[A-Z]" s))
      )
    )
  )
; string-has-upper-case-p
)

;;; 26-Feb-2012
(defun string-mixed-case-p (s)
  "Returns true if `s` contains both upper and lower case characters"
  (lse-safe
    (let ((case-fold-search nil)
         )
      (save-match-data
        (integerp (string-match "[A-Z].*[a-z]\\|[a-z].*[A-Z]" s))
      )
    )
  )
; string-mixed-case-p
)

;;;; __END__ lse-basics.el
